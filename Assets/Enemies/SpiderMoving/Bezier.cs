﻿using UnityEngine;
using System.Collections.Generic;
[RequireComponent(typeof(LineRenderer))]
public class Bezier : MonoBehaviour
{
    public Transform[] controlPoints;
    public LineRenderer lineRenderer;


    public int layerOrder = 0;
    public int segmentCount = 50;


    void Start()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        
    }

    private float _temp;
    public List<Vector3> CustomBezierLine(Vector3 start, Vector3 up, Vector3 End,int _segmentCount)
    {
        controlPoints[0].transform.position = start;
        controlPoints[1].transform.position = up;
        controlPoints[2].transform.position = End;
       // Debug.Log(Vector3.Distance(start, End) + " " + Vector3.Distance(start, up) + " " + Vector3.Distance(End, up));
        segmentCount = _segmentCount;
        return BezierLine();
    }
    public List<Vector3> BezierLine()
    {
        List<Vector3> bezierPoints = new List<Vector3>();
        for (int i = 1; i <= segmentCount; i++)
        {
            _temp = (float)i / (float)segmentCount;
            bezierPoints.Add(QuadroBezierPoint(controlPoints[0].localPosition, controlPoints[1].localPosition, controlPoints[2].localPosition, _temp ));
        }
        lineRenderer.positionCount = bezierPoints.Count;
        lineRenderer.SetPositions(bezierPoints.ToArray());
        return bezierPoints;
    }
    private Vector3 QuadroBezierPoint(Vector3 conPoint1, Vector3 conPoint2, Vector3 conPoint3,float segmentCount)
    {
        Vector3 bezierPoint=Vector3.zero;
        bezierPoint.x = Mathf.Pow((1 - segmentCount), 2) * conPoint1.x + (1 - segmentCount) * segmentCount * conPoint2.x + Mathf.Pow((segmentCount), 2) * conPoint3.x;
        bezierPoint.y = Mathf.Pow((1 - segmentCount), 2) * conPoint1.y + (1 - segmentCount) * segmentCount * conPoint2.y + Mathf.Pow((segmentCount), 2) * conPoint3.y;
        // bezierPoint.z = Mathf.Pow((1 - segmentCount), 2) * conPoint1.z + (1 - segmentCount) * segmentCount * conPoint2.z + Mathf.Pow((segmentCount), 2) * conPoint3.z;
        bezierPoint.z = Mathf.Lerp(conPoint1.z, conPoint3.z, segmentCount);
        return bezierPoint;
    }
  
}