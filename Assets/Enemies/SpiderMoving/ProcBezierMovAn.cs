﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcBezierMovAn : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] protected Transform _targetGroup, _mainPart;
    [SerializeField] protected Bezier _bezierPathCalc;
    public Vector3 direction;
    public float moveHight, speedAn;
    public int animClat;
    [Range(0, 1)] public float highPointPos = 0.5f;
    private bool _firstPairMove = true;
    private bool _movePaused = true;
    private List<Vector3>[] _legPath;
    private Transform[] _targets;
    private Rigidbody[] _targetForce;
    private Vector3 _temp, _odlDirection = Vector3.zero;
    public bool MovePaused { get { return _movePaused; } set {if(!value && direction==Vector3.zero && OldDirection==Vector3.zero)_movePaused=value;}}
    public Vector3 OldDirection { get { return _odlDirection; } }
    void Awake()
    {
        _targets = new Transform[4];
        _targetForce = new Rigidbody[4];
        _legPath = new List<Vector3>[4];
        for (int i = 0; i < _targets.Length; i++)
        {
            _targets[i] = _targetGroup.GetChild(i);
            _targetForce[i] = _targets[i].GetComponent<Rigidbody>();
            
        }

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(_odlDirection!=Vector3.zero && _movePaused)
        {
            _movePaused = false;
            MoveForOneLeg(1, _odlDirection);
            MoveForOneLeg(3, _odlDirection);
            _odlDirection = Vector3.zero;
        }
        else
        if(direction!=Vector3.zero && _movePaused)
        {
            _odlDirection = direction;
            _movePaused = false;
                MoveForOneLeg(0, _odlDirection);
                MoveForOneLeg(2, _odlDirection);
        }
        
    }
    public void MoveForOneLeg(int number,Vector3 direction)
    {
        _bezierPathCalc.transform.position = _mainPart.transform.position;
        _temp = new Vector3(Mathf.Lerp(_targets[number].position.x, _targets[number].position.x + direction.x, highPointPos), _targets[number].position.y+ moveHight, _targets[number].position.z);
        //Debug.Log(_targets[number].position.x + " " + Mathf.Lerp(_targets[number].position.x, _targets[number].position.x + direction.x, highPointPos) + " " + (_targets[number].position.x + direction.x));
        _legPath[number] = _bezierPathCalc.CustomBezierLine(_targets[number].position, _temp, _targets[number].position + direction, animClat);
        //Debug.Log((_legPath[number][0] - _legPath[number][49]) + " " + (_legPath[number][0] - _legPath[number][24]) + " " + (_legPath[number][0] - _legPath[number][1]));
        
        StartCoroutine(MovingPair(number, _legPath[number]));
    }

    private IEnumerator MovingPair(int number, List<Vector3> path)
    {
        //Vector3 _startPos;
        //_startPos = _targets[number].transform.position;
        _targetForce[number].isKinematic = true;
        int i = 1;
        while (i < animClat-1)
        {
            //to dziala
            _targets[number].Translate((path[i]-path[i-1]),Space.World);
            _mainPart.transform.Translate((path[i] - path[i - 1])/ _targets.Length, Space.World);

            //_targetForce[number].MovePosition((path[i] - path[i - 1]) + _startPos);
            i++;
            yield return new WaitForSeconds( 1/(speedAn * animClat));
        }
        _targetForce[number].isKinematic = false;
        yield return new WaitForSeconds(1 / (speedAn * animClat));
        _movePaused = true;
    }
    
}
