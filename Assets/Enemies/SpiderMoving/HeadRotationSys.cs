﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotationSys : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Transform _headBone;
    public void Rotate(float digid,int animClat, float animSpeed)
    {
        StartCoroutine(RotateHead(animClat, animSpeed, digid));
    }
    private IEnumerator RotateHead(int animClat,float animspeed,float digid)
    {
        int i = 0;
        while (i < animClat)
        {
            //to dziala
            this.transform.position = _headBone.transform.position;
            this.transform.Rotate(Vector3.up * digid / (animClat));
            _headBone.LookAt(this.transform.GetChild(0));

            //_targetForce[number].MovePosition((path[i] - path[i - 1]) + _startPos);
            i++;
            yield return new WaitForSeconds(1 / (animspeed * animClat));
        }
        
    }
}
