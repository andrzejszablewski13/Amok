﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ProcBezierMovAn))]
public class ProcBezierRotate : MonoBehaviour
{
    public enum RotateType
    {
        smallLeft,BigLeft,smallright,BigRight,fullHalf
    }
    // Start is called before the first frame update
    private ProcBezierMovAn _moveSystem;
    [SerializeField] private HeadRotationSys _rotateHead;
    public bool rotate;
    private int _posNumber=0;
    public RotateType typeOfRotate;
    private float _vecMultipliyer = 1.75f;
    void Start()
    {
        _moveSystem = this.GetComponent<ProcBezierMovAn>();
    }
    private Vector3[] _legsMovementToRotate = new Vector3[4] { Vector3.right , -Vector3.forward , -Vector3.right , Vector3.forward  },
        _legsMovementToRotate2 = new Vector3[4] {Vector3.forward+ Vector3.right, Vector3.right - Vector3.forward,  -Vector3.right - Vector3.forward, Vector3.forward-
            Vector3.right };
    // Update is called once per frame
    void LateUpdate()
    {
        if(_moveSystem.direction==Vector3.zero && _moveSystem.MovePaused && _moveSystem.OldDirection==Vector3.zero && rotate)
        {
            _moveSystem.MovePaused = false;
            switch (typeOfRotate)
            {
                case RotateType.smallLeft:
                    for (int i = 0; i < _legsMovementToRotate.Length; i++)
                    {
                        _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate[i] * _vecMultipliyer);
                    }
                    _rotateHead.Rotate(-45,_moveSystem.animClat,_moveSystem.speedAn);
                    _posNumber++;
                    _posNumber = _posNumber > 7 ? 0 : _posNumber;
                    break;
                case RotateType.BigLeft:
                    if(_posNumber%2==0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate[i] * _vecMultipliyer*2);
                        }
                        _rotateHead.Rotate(-90, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber+=2;
                        _posNumber = _posNumber > 7 ? _posNumber-8 : _posNumber;
                    }else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate2[i] * _vecMultipliyer);
                        }
                        _rotateHead.Rotate(-90, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber += 2;
                        _posNumber = _posNumber > 7 ? _posNumber - 8 : _posNumber;
                    }
                    break;
                case RotateType.smallright:
                    for (int i = 0; i < _legsMovementToRotate.Length; i++)
                    {
                        _moveSystem.MoveForOneLeg(NumOfLeg(NumOfDirInf(i, 1), false), _legsMovementToRotate[i] * _vecMultipliyer);
                    }
                    _rotateHead.Rotate(45, _moveSystem.animClat, _moveSystem.speedAn);
                    _posNumber--;
                    _posNumber = _posNumber < 0 ? 7 : _posNumber;
                    break;
                case RotateType.BigRight:
                    if (_posNumber % 2 == 0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(NumOfDirInf(i, 1), false), _legsMovementToRotate[i] * _vecMultipliyer*2);
                        }
                        _rotateHead.Rotate(90, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber-=2;
                        _posNumber = _posNumber < 0 ? 8+ _posNumber : _posNumber;
                    }
                    else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate2[i] * _vecMultipliyer);
                        }
                        _rotateHead.Rotate(90, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber -= 2;
                        _posNumber = _posNumber < 0 ? 8 + _posNumber : _posNumber;
                    }
                        break;
                case RotateType.fullHalf:
                    if (_posNumber % 2 == 0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate2[i] * _vecMultipliyer * 2);
                        }

                        _rotateHead.Rotate(180, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber -= 4;
                        _posNumber = _posNumber < 0 ? 8 + _posNumber : _posNumber;
                    }
                    else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate[i] * _vecMultipliyer * 2);
                        }
                        _rotateHead.Rotate(180, _moveSystem.animClat, _moveSystem.speedAn);
                        _posNumber -= 4;
                        _posNumber = _posNumber < 0 ? 8 + _posNumber : _posNumber;
                    }
                    break;
                default:
                    break;
            }
            
                rotate = false;
        }
        
    }
    private int NumOfLeg(int i, bool plus=true)
    {
        if (plus)
        {
            return (_posNumber / 2) + i > 3 ? (_posNumber / 2) + i - 4 : (_posNumber / 2) + i;
        }else
        {
            return ((_posNumber + 1) / 2) + i > 3 ? ((_posNumber + 1) / 2) + i - 4 : ((_posNumber + 1) / 2) + i;
        }
    }
    private int NumOfDirInf(int i,int dif)
    {
        return (i+ dif) % _legsMovementToRotate.Length;
    }
}
