﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class SlangBlendAI : BaseEnemy
    {
        [SerializeField] private DetectCollisionWitchPalyer _attackedPlayer;
        [SerializeField] private float _distanceToAttack = 2;
        private Animator _anim;
        private bool _attacked = false;
        public override void Ready()
        {
            _anim = this.GetComponent<Animator>();
            base.Ready();
            _detColl.myDetPlayer -= DoDmgMeele;
            _attackedPlayer.myDetPlayer+= DoDmgMeele;
            _aIControler.stoppingDistance = _distanceToAttack*0.5f;
        }
        protected override void OnDisable()
        {
            _attackedPlayer.myDetPlayer -= DoDmgMeele;
            _anim.SetBool("playerInSide", false);
            base.OnDisable();
        }
        public override void DoAction()
        {
            if (_playerInSide && Vector3.Distance(_player.transform.position, this.transform.position) > _distanceToAttack)
            {
                _aIControler.SetDestination(_player.transform.position);
                _anim.SetBool("playerInSide", true);
            }else if(Vector3.Distance(_player.transform.position, this.transform.position) < _distanceToAttack)
            {
                this.transform.rotation = Quaternion.LookRotation(
                    new Vector3(_player.transform.position.x, this.transform.position.y, _player.transform.position.z) - this.transform.position);
                _attacked = false;
                _anim.SetTrigger("attack");
            }
            else
            {
                _anim.SetBool("playerInSide", false);
            }
        }
        protected override void DoDmgMeele(IHitByMeele hittedPlayer)
        {
            if(!_attacked)
            {
                _attacked = true;
                hittedPlayer.TakeDmgClose(dmg);
            }
            
        }
    }
}