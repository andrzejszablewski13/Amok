﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HideOnClick : MonoBehaviour,IPointerUpHandler,IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
	    // Debug.Log("test2");
	    this.gameObject.SetActive(false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        this.gameObject.SetActive(false);
	    // Debug.Log("test");
    }
}
