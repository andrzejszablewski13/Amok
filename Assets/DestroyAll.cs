﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAll : MonoBehaviour
{
	private DoomWave.IHitByMeele _toKill;
	private int _ultraDMG=10000;
// Start is called before the first frame update
	// OnTriggerEnter is called when the Collider other enters the trigger.
	protected void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.TryGetComponent(out _toKill))
		{
			_toKill.TakeDmgClose(_ultraDMG);
		}
	}
}