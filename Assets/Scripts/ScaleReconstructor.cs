﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleReconstructor : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        this.transform.localScale = new Vector3(1 / this.transform.parent.localScale.x, 1 / this.transform.parent.localScale.y, 1 / this.transform.parent.localScale.z);
    }

}
