﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	public class PlayerNumbJumpUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				base._playerData.data.maxNumberJump +=1* _upOrDownVariable;
				base.EndingRewardSystem();
			}
		}
	}
}
