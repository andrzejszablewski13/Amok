﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	public class LineralPlayerDashUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
			base._playerData.data.daschPower += base._modsData.lineralData.playerDaschPower* _upOrDownVariable;
				base.EndingRewardSystem();
			}
		}
	}
}
