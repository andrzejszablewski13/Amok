﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	public class LineralPlayerHpUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
			base._playerData.data.maxHp +=(int) base._modsData.lineralData.playerMaxHp* _upOrDownVariable;
				base._playerData.data.hp +=(int)base._modsData.lineralData.playerMaxHp* _upOrDownVariable;
				TakingDmg.OnhpChange();
				base.EndingRewardSystem();
			}
		}
	}
}
