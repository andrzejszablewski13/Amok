﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DoomWave
{
    public class LineralPlayerSpeedUp : BaseUpgrade
    {
        public override void SetButton()
        {
            for (int i = 0; i < base._loop; i++)
            {
                base._playerData.data.maxSpeed += base._modsData.lineralData.playerSpeed* _upOrDownVariable;
                base.EndingRewardSystem();
            }
        }
    }
}