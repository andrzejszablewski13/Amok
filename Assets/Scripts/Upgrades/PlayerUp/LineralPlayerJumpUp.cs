﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	public class LineralPlayerJumpUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
			base._playerData.data.jumpPower += base._modsData.lineralData.playerJumpPower* _upOrDownVariable;
				base.EndingRewardSystem();
			}
		}
	}
}
