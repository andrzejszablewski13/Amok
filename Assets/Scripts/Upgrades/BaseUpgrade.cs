﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace DoomWave
{
    public class BaseUpgrade : MonoBehaviour
    {
        // Start is called before the first frame update
        protected WeaponData _weaponData;
        protected BaseEnemiesStat _enemiesData;
        protected ModifiersData _modsData, _modsGrowData;
        protected PlayerData _playerData;
        protected GameObject _mainObject, _weaponSettings;
        protected Button _button;
        protected TextMeshProUGUI _textField;
        protected UnityAction _buttonAction;
        protected GameObject _player;
        protected BasicMovement _basicPlayerMove;
        protected int _loop;
        protected bool _upOrDown;
        protected int _upOrDownVariable;
        public virtual void SetData(Button button, TextMeshProUGUI textField,GameObject weaponSettings, GameObject mainObject, int loop=1, bool upOrDown=true)
        {
            _buttonAction = new UnityAction(SetButton);
            _mainObject = mainObject;
            _button = button;
            _textField = textField;
            _loop = loop;
            _upOrDown = upOrDown;
            _weaponSettings = weaponSettings;
            _upOrDownVariable = upOrDown ? 1 : -1;
            if (_weaponData == null)
            {
                _weaponData = PrimalClass.Singleton.GetWEaponData();
                _enemiesData = PrimalClass.Singleton.GetBaseEnemiesStat();
                _modsData = PrimalClass.Singleton.GetModifiersData();
                _modsGrowData = PrimalClass.Singleton.GetModifiersDataGrowth();
                _playerData = PrimalClass.Singleton.GetPlayerData();
            }
            _button.onClick.AddListener(_buttonAction);
            _player = PrimalClass.Singleton.GetPlayer();
            _basicPlayerMove = _player.GetComponent<BasicMovement>();
            _textField.text += this.name+" ";
            
        }
        public virtual void SetButton()
        {

        }
        protected void EndingRewardSystem()
	    {
            _mainObject.SetActive(false);
            _button.onClick.RemoveListener(_buttonAction);
        }
    }
}