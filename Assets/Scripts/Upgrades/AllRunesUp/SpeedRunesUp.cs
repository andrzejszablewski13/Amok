﻿
namespace DoomWave
{
	public class SpeedRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].speedOfBullet *= base._weaponData.runes[j].modBulletSpeed;
					}
					else
					{
						base._weaponData.runes[j].speedOfBullet /= base._weaponData.runes[j].modBulletSpeed;
					}
				}

				
			}
			base.EndingRewardSystem();
		}
	}
}