﻿
namespace DoomWave
{
	public class RecoilRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].loadingManaTime *= base._weaponData.runes[j].modOfRecail;
					}
					else
					{
						base._weaponData.runes[j].loadingManaTime /= base._weaponData.runes[j].modOfRecail;
					}
				}
			}
			base.EndingRewardSystem();
		}
	}
}