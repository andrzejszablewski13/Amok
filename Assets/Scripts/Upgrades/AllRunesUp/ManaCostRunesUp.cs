﻿
namespace DoomWave
{
	public class ManaCostRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].manaCost *= base._weaponData.runes[j].modOfManaCost;
					}
					else
					{
						base._weaponData.runes[j].manaCost /= base._weaponData.runes[j].modOfManaCost;
					}
				}
			}
			base.EndingRewardSystem();
		}
	}
}