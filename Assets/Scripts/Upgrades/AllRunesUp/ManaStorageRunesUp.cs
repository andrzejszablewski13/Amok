﻿
namespace DoomWave
{
	public class ManaStorageRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].manaStorage *= base._weaponData.runes[j].modOfManaStorage;
					}
					else
					{
						base._weaponData.runes[j].manaStorage /= base._weaponData.runes[j].modOfManaStorage;
					}
				}
			}
			base.EndingRewardSystem();
		}
	}
}