﻿
namespace DoomWave
{
	public class ScatterRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].wScatter *= base._weaponData.runes[j].wScatterMod;
					}
					else
					{
						base._weaponData.runes[j].wScatter /= base._weaponData.runes[j].wScatterMod;
					}
				}
			}
		base.EndingRewardSystem();
		}
	}
}