﻿
namespace DoomWave
{
	public class DmgRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
                for (int j = 0; j < base._weaponData.runes.Length; j++)
                {
					if (_upOrDown)
					{
						base._weaponData.runes[j].dmgChange *= base._weaponData.runes[j].modOfDmg;
					}else
                    {
						base._weaponData.runes[j].dmgChange /= base._weaponData.runes[j].modOfDmg;
					}

				}
				
			}
			base.EndingRewardSystem();
		}
	}
}