﻿
namespace DoomWave
{
	public class ManaChangeCostUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].manaChange *= base._weaponData.runes[j].modOfManaCharge;
					}
					else
					{
						base._weaponData.runes[j].manaChange /= base._weaponData.runes[j].modOfManaCharge;
					}
				}
			}
			base.EndingRewardSystem();
		}
	}
}