﻿
namespace DoomWave
{
	public class KnockBackRunesUp : BaseUpgrade
	{
		public override void SetButton()
		{
			for (int i = 0; i < base._loop; i++)
			{
				for (int j = 0; j < base._weaponData.runes.Length; j++)
				{
					if (_upOrDown)
					{
						base._weaponData.runes[j].knockBack *= base._weaponData.runes[j].modKnockBack;
					}
					else
					{
						base._weaponData.runes[j].knockBack /= base._weaponData.runes[j].modKnockBack;
					}

				}
			}
		base.EndingRewardSystem();
		}
	}
}