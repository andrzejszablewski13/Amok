﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	public class TakingDmg : MonoBehaviour, IHitByMeele,IHitByRange
    {
	    // to take dmg as palyer, for now only by meele
	    [SerializeField] private PlayerData _playerData;
        [SerializeField] private TMPro.TextMeshProUGUI _hpText;
        public delegate void HpChange();
        public static HpChange OnhpChange;
        public float _inviTime = 0.3f;
        private float _time=0;
        private void OnEnable()
        {
            OnhpChange += ShowAcHp;
        }
        private void OnDisable()
        {
            OnhpChange -= ShowAcHp;
        }
        private void Start()
        {
            ShowAcHp();
        }
        private void ShowAcHp()
        {
            _hpText.text ="HP: "+ _playerData.data.hp + " / " + _playerData.data.maxHp;
        }

        public void TakeDmgClose(int value)
        {
            if (_time <= Time.time)
            {
	            _playerData.data.hp	 -= value;
                _time = Time.time + _inviTime;
            }
            if(_playerData.data.hp<=0)
            {
	            this.GetComponent<ChangeScene>().ChangeSceneNow();
            }
            OnhpChange();
        }
	    public void TakeDmg(int value)
	    {
		    if (_time <= Time.time)
		    {
			    _playerData.data.hp	 -= value;
			    _time = Time.time + _inviTime;
		    }
		    if(_playerData.data.hp<=0)
		    {
			    this.GetComponent<ChangeScene>().ChangeSceneNow();
		    }
		    OnhpChange();
	    }
        
    }
}