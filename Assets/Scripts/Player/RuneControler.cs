﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using PathologicalGames;
namespace DoomWave
{
	//to control weapons made from moduls
    public class RuneControler : MonoBehaviour
    {

        [SerializeField] private GameObject _preFab,_middlePoint;
        [SerializeField] private string _nameOfType;
        public WeaponData _data, _actualData;
        public PlayerData _player;
        private int[] _numberOfRunesInSeq;
        [HideInInspector] public Transform[] _runes;
        protected RaycastHit _hit;
        protected Ray _ray;
        [HideInInspector]public int _runeID = 0;
        public delegate void FireWeapon(bool startStop);
        public FireWeapon firing;
        public delegate void PreLoadWep(bool startStop);
        public PreLoadWep preLoadingWeapon;
        public bool _readyToFire = true;
        private void Start()
        {
            DetectRunes();
            ResetStat();//for debuging
            if (_actualData.runes.Length==0 || _actualData.runes[0].dmgChange==0)
            {
                ResetStat();
            }
           

        }
        public void DetectRunes()
        {
            _runes = null;
            _runes = new Transform[this.transform.childCount];
            for (int i = 0; i < this.transform.childCount; i++)
            {
                _runes[i] = this.transform.GetChild(i);
            }
            _numberOfRunesInSeq = new int[_runes.Length];
            for (int i = 0; i < _runes.Length; i++)
            {
                for (int j = 0; j < _actualData.runes.Length; j++)
                {
                    if (_actualData.runes[j].name.ToString() == _runes[i].name)
                    {
                        _numberOfRunesInSeq[i] = j;
                    }
                }
            }
        }
        
        public void OnFire1(InputAction.CallbackContext context)
        {
            if (context.ReadValueAsButton() && _readyToFire)
            {
                // StartCoroutine(_corutinFire1);
                firing(true);
            }
            if (context.canceled)
            {
                firing(false);
            }
        }
        public void GetPosition(InputAction.CallbackContext context)
        {
            //_ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (_middlePoint == null)
            {
                _ray = Camera.main.ScreenPointToRay(context.ReadValue<Vector2>());

                Debug.Log(_ray);
                Debug.DrawRay(_ray.origin, _ray.direction * 100, Color.green);
                if (Physics.Raycast(_ray, out _hit) && _readyToFire)
                {
                    this.transform.LookAt(_hit.point);
                }
            }
            else
            {
                this.transform.LookAt(_middlePoint.transform.position);
            }
        }
        public void PreLoad(InputAction.CallbackContext context)
        {

            if (context.ReadValueAsButton() && _readyToFire)
            {
                // StartCoroutine(_corutinFire1);
                preLoadingWeapon(true);
            }
            if (context.canceled)
            {
                preLoadingWeapon(false);
            }
        }
        public void HideWeapon(bool yesno)
        {
            _readyToFire = !yesno;
                for (int i = 0; i < _runes.Length; i++)
                {
                _runes[i].GetComponent<DoomEffects.RuneConnector>().HideCrystals(yesno, this.transform.parent.position);
                }


        }
        public void ResetStat()
        {
            _actualData.runes = null;
            _actualData.runes = new RuneDataStack[_data.runes.Length];
             _data.runes.CopyTo(_actualData.runes,0);
        }
        protected float _timeToLive = 5;
        private Vector3 _rotateToZero;
        public void SetBullet(float dmg,float speed,Transform parent,float knockBack=10,float horScatter=0, float verScatter= 0)
        {
            _rotateToZero = parent.transform.eulerAngles - parent.transform.localEulerAngles;
            _rotateToZero.x +=Random.Range(-horScatter, horScatter); _rotateToZero.y +=Random.Range(-verScatter, verScatter);
             Transform _temp = PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Spawn(_preFab, parent.transform.position, Quaternion.Euler(_rotateToZero));
            _temp.GetComponent<BulletDmg>().dmg = Mathf.RoundToInt(dmg);
            _temp.GetComponent<BulletDmg>().knockBack = knockBack;
           _temp.GetComponent<BulletDmg>().TimeToDie = _timeToLive;
            _temp.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * speed, ForceMode.Impulse);
        }

    }
}