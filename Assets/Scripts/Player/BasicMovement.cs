﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BasicMovement : MonoBehaviour
{
	// basic movement, dasch, jump, camera follow, and cursor lock/visibility
    private Rigidbody _rb;
    [SerializeField] private float _speed = 5,_jumpForce=10;
    [SerializeField] private PlayerData _thisPlayerData, _default;
    [SerializeField] private ModifiersData _mods, _modsCopy, _modsGrowth, _modsGrowthCopy;
    private Vector2 _move=Vector2.zero;
    public float sensitivity = 0.1f;
    public float maxYAngle = 80f;
    private Vector2 _currentRotation ,_mousePos = Vector2.zero;
    private RaycastHit _hit;

    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody>();
        ResetData();
        if (_thisPlayerData.data.maxHp==0)
        {
            ResetData();
        }
    }
    private void ResetData()
    {
        _thisPlayerData.data = _default.data;
        _mods.lineralData = _modsCopy.lineralData;
        _mods.geometricData = _modsCopy.geometricData;
        _modsGrowth.lineralData = _modsGrowthCopy.lineralData;
        _modsGrowth.geometricData = _modsGrowthCopy.geometricData;
    }
    IEnumerator RepatSetCursor(bool visible)
    {
        yield return new WaitForEndOfFrame();
        Cursor.visible = visible;
    }
    public void OnMove(InputAction.CallbackContext context)
    {
        _move = context.ReadValue<Vector2>();
    }
    public void OnExit(InputAction.CallbackContext context)
    {
        Application.Quit();
    }
    public void OnMousePos(InputAction.CallbackContext context)
    {
        _mousePos = context.ReadValue<Vector2>();
    }
    public void OnFire1(InputAction.CallbackContext _)
    {

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        StartCoroutine(RepatSetCursor(false));
    }
    public void OnFire2(InputAction.CallbackContext _)
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        StartCoroutine(RepatSetCursor(true));
    }
    public void OnFire3(InputAction.CallbackContext _)
    {
        _rb.velocity = Vector3.zero;
    }
    private bool _grounded;
    public void OnJump(InputAction.CallbackContext _)
    {
        if (_grounded)
        { _thisPlayerData.data.jumpNumber = _thisPlayerData.data.maxNumberJump; }
        if (_thisPlayerData.data.jumpNumber>0)
        { 
            _rb.AddForce(Vector3.up * _thisPlayerData.data.jumpPower, ForceMode.Impulse);
            _thisPlayerData.data.jumpNumber--;
        }
        _grounded = false;
    }
    public void OnDodge(InputAction.CallbackContext _)
    {
        _rb.velocity = new Vector3(_rb.velocity.x * _thisPlayerData.data.daschPower, _rb.velocity.y, _rb.velocity.z * _thisPlayerData.data.daschPower);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _grounded = false;
        _rb.AddRelativeForce(new Vector3(_move.x, 0, _move.y) * _thisPlayerData.data.maxSpeed * (Time.deltaTime / Time.fixedDeltaTime), ForceMode.Impulse);
         _currentRotation.x += _mousePos.x* _thisPlayerData.data.mouseSensivity;
         _currentRotation.y -= _mousePos.y * _thisPlayerData.data.mouseSensivity;
        _currentRotation.x = Mathf.Repeat(_currentRotation.x, 360);
        _currentRotation.y = Mathf.Clamp(_currentRotation.y, -_thisPlayerData.data.maxYAngle, _thisPlayerData.data.maxYAngle);
        Camera.main.transform.rotation = Quaternion.Euler(_currentRotation.y, _currentRotation.x, 0);
        this.gameObject.transform.rotation = Quaternion.Euler(0, _currentRotation.x, 0);
    }
    private void OnTriggerStay(Collider _)
    {
        _grounded = true;
    }
}
