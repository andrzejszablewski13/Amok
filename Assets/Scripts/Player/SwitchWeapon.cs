﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

namespace DoomWave
{
    public class SwitchWeapon : MonoBehaviour
    {
        // switch from meele to rune weapon by few ways
        private GameObject[] _weapons;
        private int _iDWeapon = 0;
        private int _weaponsUnBlock = 1;
        [SerializeField] private TextMeshProUGUI _textAmmo;
        [SerializeField] private PlayerData _player;
        [SerializeField] private RuneControler _controler;
        public int IDWeapon{get {return _iDWeapon;} }
        public int WeaponsUnBlock
        {
            get { return _weaponsUnBlock; }
            set
            {
                _weaponsUnBlock = value;
                ChangeWeaponByNumber(value - 1);
            }
        }
        private void Start()
        {
            _weapons = new GameObject[this.transform.childCount-1];
            for (int i = this.transform.childCount - 2; i >= 0; i--)
            {
                _weapons[i] = this.transform.GetChild(i).gameObject;
                _weapons[i].SetActive(false);
            }
            _weapons[_iDWeapon].SetActive(true);
            
        }
        private void OnEnable()
        {
            Keyboard.current.onTextInput += ChangeWeaponByNumber;
        }
        private void OnDisable()
        {
            Keyboard.current.onTextInput -= ChangeWeaponByNumber;
        }
        private string ammoNumber;
        private void FixedUpdate()
        {
            ammoNumber = _iDWeapon > 0 ? _player.data.manaStack.ToString() + " / " + _player.data.maxManaStack.ToString():"Knife";
            _textAmmo.text = ammoNumber;
        }
        // Update is called once per frame

        public void OnScrollWeapon(InputAction.CallbackContext contex)
        {
            if (contex.ReadValue<Vector2>().y > 0)
            {
                if (_iDWeapon == 0)
                {
                    _weapons[_iDWeapon].SetActive(false);
                }
                else
                {
                    _controler.HideWeapon(true);
                }
                _iDWeapon = _iDWeapon == _weaponsUnBlock - 1 ? 0 : _iDWeapon + 1;
                if (_iDWeapon == 0)
                {
                    _weapons[_iDWeapon].SetActive(true);
                }
                else
                {
                    _controler.HideWeapon(false);
                }

            }
            else if (contex.ReadValue<Vector2>().y < 0)
            {
                if (_iDWeapon == 0)
                {
                    _weapons[_iDWeapon].SetActive(false);
                }
                else
                {
                    _controler.HideWeapon(true);
                }
                _iDWeapon = _iDWeapon == 0 ? _weaponsUnBlock - 1 : _iDWeapon - 1;
                if (_iDWeapon ==0)
                {
                    _weapons[_iDWeapon].SetActive(true);
                }
                else
                {
                    _controler.HideWeapon(false);
                }
            }
        }
        private void ChangeWeaponByNumber(int i)
        {
            _weapons[_iDWeapon].SetActive(false);
            _iDWeapon = i;
            _weapons[_iDWeapon].SetActive(true);
        }
        private string _temp;


        private void ChangeWeaponByNumber(char _temp)
        {
            switch (_temp)
            {
                case '1' :
                    ChangeWeaponByNumber(0);
                    // Debug.Log(1);
                    break;
                case '2'  :
                    if (_weaponsUnBlock >= 2)
                        ChangeWeaponByNumber(1);
                    // Debug.Log(2);
                    break;
                default:
                    break;
            }
        }
    }
}