﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DoomWave
{
    public class MeeleAttack : MonoBehaviour
	{
		//meele attack by player
        [SerializeField] private PlayerData _data;
 
        private Animator _anim;
		private string _attack="AttackMele2",_attack2 = "sword|attack_fullsequence";
        void Start()
        {
            _anim = this.GetComponent<Animator>();
        }
	    public void OnFire1(InputAction.CallbackContext _)
	    {
            if (this.gameObject.activeInHierarchy)
            {
                _anim.Play(_attack);
            }
	    }
		public void OnFire2(InputAction.CallbackContext _)
		{
			if (this.gameObject.activeInHierarchy)
			{
				_anim.Play(_attack2);
			}
		}

        private IHitByMeele _temp;
        private void OnTriggerEnter(Collider other)
        {
            if(other.TryGetComponent(out _temp))
            {
                _temp.TakeDmgClose(_data.data.dmg);
            }
        }
    }
}