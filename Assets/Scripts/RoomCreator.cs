﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DoomWave
{
    public class RoomCreator : MonoBehaviour
    {
        private enum Blocks
        {
            Room, BrefRoom, HidenRoom, TestEnemy, Wall, AroundDoor
        }
        [SerializeField] private EnemySpawner _spawner;
        [SerializeField] private RoomControler _lastRoom, _lastBrefRoom;
        private Transform _newRoom, _newBrefRoom;
        private Transform _oldRoom, _oldBrefRoom;


        public Vector3 roomSize = new Vector3(26, 12, 25);
        public Vector3 doorSize = new Vector3(3, 5, 0.9f);

        void FixedUpdate()
	    {
		    //generate new bref room i despawn old if player is in correct place
            if (_lastBrefRoom.playerInThisRoom)
            {
                _newBrefRoom = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.BrefRoom.ToString(), _lastRoom.door.transform.position, Quaternion.identity);
                _newBrefRoom.GetComponent<RoomControler>().door.GetComponent<DoorControler>().ReadyToOpen = true;
                if (_oldBrefRoom != null && !_oldBrefRoom.gameObject.name.Contains("Tutorial"))
                {
                    _oldBrefRoom.GetComponent<RoomControler>().playerInThisRoom = false;
                    
                        PoolManager.Pools[PopularStrings.Rooms.ToString()].Despawn(_oldBrefRoom);
                }

                _oldBrefRoom = _lastBrefRoom.transform;
                _lastBrefRoom = _newBrefRoom.GetComponent<RoomControler>();

            }
		    //generate new fight room i despawn old if player is in correct place
            if (_lastRoom.playerInThisRoom)
            {
                _newRoom = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Room.ToString(), _lastBrefRoom.door.transform.position, Quaternion.identity);
                if (_oldRoom != null && !_oldRoom.gameObject.name.Contains("Tutorial"))
                {
                    RoomDespawner(_oldRoom);
                }
                _oldRoom = _lastRoom.transform;
                _lastRoom = _newRoom.GetComponent<RoomControler>();
                RoomSpawner(_lastRoom);
            }
        }
	    
        private void RoomSpawner(RoomControler _room)
        {
            _room.door.transform.localPosition = new Vector3(0, 0, roomSize.z - 1);
            _room.flor.transform.position = new Vector3(_room.door.transform.position.x, 0, _room.door.transform.position.z- (roomSize.z-1) / 2);
            _room.flor.transform.localScale = new Vector3(roomSize.x, 1, roomSize.z);
            _room.roof.transform.position = new Vector3(_room.door.transform.position.x, roomSize.y-1, _room.flor.transform.position.z);
            _room.roof.transform.localScale = new Vector3(roomSize.x, 1, roomSize.z);
            PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.AroundDoor.ToString(), _room.door.transform.position, Quaternion.identity, _room.walls.transform);
            CubeRoom(_room);

            //for nave mesch-better on the end
            _room.flor.GetComponent<NavMeshSurface>().BuildNavMesh();
            _spawner.BaseEnemySpawn(_room);
        }
	    private float _placeForWalls,_quarterOfCircle=90;
	    //generate clear room witch cube size
        private void CubeRoom(RoomControler _room)
        {
            
            Transform _temp;
            //for back wall
            _placeForWalls = (roomSize.x / 2 - doorSize.x - 1 - 1);
            for (int i = 0; i < _placeForWalls;)
            {
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3((doorSize.x + _temp.localScale.z*2) + _temp.localScale.x / 2 + i
                    , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                    , 0);
                _temp.localEulerAngles = Vector3.zero;
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3(-(doorSize.x + _temp.localScale.z*2) - _temp.localScale.x / 2 - i
                    , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                    , 0);
                _temp.localEulerAngles = Vector3.zero;
                i += (int)_temp.localScale.x;
            }
            //for left and right wall
            _placeForWalls = roomSize.z-1;
            for (int i = 0; i < _placeForWalls;)
            {
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3((roomSize.x- _temp.localScale.z) /2
                   , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                   , _temp.localScale.z/2+ _temp.localScale.x/2+i);
                _temp.localEulerAngles = Vector3.up * _quarterOfCircle;
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3(-(roomSize.x - _temp.localScale.z) / 2
                   , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                   , _temp.localScale.z/2 + _temp.localScale.x / 2 + i);
                _temp.localEulerAngles = Vector3.up * _quarterOfCircle;
                i += (int)_temp.localScale.x;
            }
            //for forward wall
            _placeForWalls = (roomSize.x / 2 - doorSize.x - 1 - 1);
            for (int i = 0; i < _placeForWalls;)
            {
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3((doorSize.x + _temp.localScale.z) + _temp.localScale.x / 2 + i
                    , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                    , _room.flor.transform.localScale.z-1);
                _temp.localEulerAngles = Vector3.zero;
                _temp = PoolManager.Pools[PopularStrings.Rooms.ToString()].Spawn(Blocks.Wall.ToString(), _room.gameObject.transform.position, Quaternion.identity, _room.walls.transform);
                _temp.transform.localPosition = new Vector3(-(doorSize.x + _temp.localScale.z) - _temp.localScale.x / 2 - i
                    , _room.flor.transform.localScale.y / 2 + _room.flor.transform.localPosition.y + _temp.localScale.y / 2
                    , _room.flor.transform.localScale.z - 1);
                _temp.localEulerAngles = Vector3.zero;
                i += (int)_temp.localScale.x;
            }
        }
        private void RoomDespawner(Transform _room)
        {
            RoomControler _temp = _room.GetComponent<RoomControler>();
            for (int i = _temp.walls.transform.childCount - 1; i >= 0; i--)
            {
                if(_temp.walls.transform.GetChild(i).transform.name== Blocks.AroundDoor.ToString())
                {
                    PoolManager.Pools[PopularStrings.Rooms.ToString()].Despawn(_temp.walls.transform.GetChild(i));
                }
                else if(_temp.walls.transform.GetChild(i).transform.name == Blocks.Wall.ToString())
                {
                    PoolManager.Pools[PopularStrings.Rooms.ToString()].Despawn(_temp.walls.transform.GetChild(i));
                }
            }
            
            _temp.playerInThisRoom = false;
            PoolManager.Pools[PopularStrings.Rooms.ToString()].Despawn(_room);
        }
    }
    
}