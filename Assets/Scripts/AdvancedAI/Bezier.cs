﻿using UnityEngine;
using System.Collections.Generic;
[RequireComponent(typeof(LineRenderer))]
public class Bezier : MonoBehaviour
{
    public Transform[] controlPoints;
    public LineRenderer lineRenderer;


    public int layerOrder = 0;
    public int segmentCount = 50;


    private float _temp;
    private int _count;
    void Start()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        
    }

    public List<Vector3> CustomBezierLine(Vector3 start, Vector3 up, Vector3 End,int _segmentCount)
    {
        _count = -1;
        controlPoints[++_count].transform.position = start;
        controlPoints[++_count].transform.position = up;
        controlPoints[++_count].transform.position = End;
        segmentCount = _segmentCount;
        return BezierLine();
    }
    public List<Vector3> BezierLine()
    {
        List<Vector3> bezierPoints = new List<Vector3>();
        for (int i = 1; i <= segmentCount; i++)
        {
            _temp = (float)i / (float)segmentCount;
            _count = -1;
            bezierPoints.Add(QuadroBezierPoint(controlPoints[++_count].localPosition, controlPoints[++_count].localPosition, controlPoints[++_count].localPosition, _temp ));
        }
        lineRenderer.positionCount = bezierPoints.Count;
        lineRenderer.SetPositions(bezierPoints.ToArray());
        return bezierPoints;
    }
    private Vector3 QuadroBezierPoint(Vector3 conPoint1, Vector3 conPoint2, Vector3 conPoint3,float segmentCount)
    {
        Vector3 bezierPoint=Vector3.zero;
        bezierPoint.x = Mathf.Pow((1 - segmentCount), 2) * conPoint1.x + (1 - segmentCount) * segmentCount * conPoint2.x + Mathf.Pow((segmentCount), 2) * conPoint3.x;
        bezierPoint.y = Mathf.Pow((1 - segmentCount), 2) * conPoint1.y + (1 - segmentCount) * segmentCount * conPoint2.y + Mathf.Pow((segmentCount), 2) * conPoint3.y;
        bezierPoint.z = Mathf.Lerp(conPoint1.z, conPoint3.z, segmentCount);
        return bezierPoint;
    }
  
}