﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderScripts : MonoBehaviour
{
	public ProcBezierMovAn _movScript;
	public ProcBezierRotate _rotateScript;
	public GameObject eyeL,eyeR;
}