﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisionWitchPalyer : MonoBehaviour
{
	// Start is called before the first frame update
	[HideInInspector]public bool hitted=false;
	[HideInInspector]public DoomWave.IHitByMeele hittedPlayer;
	public delegate void DetectCollisionPlayer(DoomWave.IHitByMeele hittedPlayer);
	public DetectCollisionPlayer myDetPlayer;


    // OnTriggerEnter is called when the Collider other enters the trigger.
    protected void OnTriggerEnter(Collider other)
	{
		if (other.tag =="Player" && other.TryGetComponent(out hittedPlayer)&& myDetPlayer!=null)
		{
			
			myDetPlayer(hittedPlayer);
			hitted = true;
			
		}
	}
}