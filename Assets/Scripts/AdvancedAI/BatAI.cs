﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class BatAI : BaseEnemy
    {
        private float _timeToAttack;
        private Animator _anim;
        private Rigidbody _rB;
        private string _anName = "Attacking";
        private float _instaKill = 1000, _minTimeToAtt = 2, _maxTimeToAtt = 16, _timeToMakeDec=1.5f,_speedMod=5;
        protected override void OnDisable()
        {
            if (_ready)
            {
                _rB.velocity = Vector3.zero;

                base.OnDisable();
            }
        }
        public override void Ready()
        {
            _rB = this.GetComponent<Rigidbody>();
            base.Ready();
            _timeToAttack = Random.Range(_minTimeToAtt, _maxTimeToAtt);
            _anim = this.GetComponent<Animator>();
            _timeToResetDest = _timeToMakeDec;
            _aIControler.enabled = false;
            
        }
        public override void DoAction()
        {
            if(_playerInSide)
            {
                _timeToAttack--;
                if(_timeToAttack<0)
                {
                    this.transform.LookAt(_player.transform.position);
                    _anim.SetBool(_anName, true);
                }else
                {
                    this.transform.Rotate(Vector3.up * _aIControler.speed* _speedMod*3);
                    _anim.SetBool(_anName, false);
                }
            }
            _rB.AddRelativeForce(Vector3.forward * _aIControler.speed* _speedMod*2, ForceMode.Impulse);
        }
        protected override void DoDmgMeele(IHitByMeele hittedPlayer)
        {
            base.DoDmgMeele(hittedPlayer);
            this.TakeDmgClose((int)_instaKill);
        }
    }
}