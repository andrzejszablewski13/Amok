﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ProcBezierMovAn))]
public class ProcBezierRotate : MonoBehaviour
{
    public enum RotateType
    {
        smallLeft,BigLeft,smallright,BigRight,fullHalf
    }
    // Start is called before the first frame update
    private ProcBezierMovAn _moveSystem;
    [SerializeField] private HeadRotationSys _rotateHead;
    public bool rotate;
    private int _posNumber=0;
    public RotateType typeOfRotate;
    private float _vecMultipliyer = 1.75f;
    private int _halfQouaterCircle = 45, _legsPosPosition = 8;
    void Start()
    {
        _moveSystem = this.GetComponent<ProcBezierMovAn>();
    }
    private Vector3[] _legsMovementToRotate = new Vector3[4] { Vector3.right , -Vector3.forward , -Vector3.right , Vector3.forward  },
        _legsMovementToRotate2 = new Vector3[4] {Vector3.forward+ Vector3.right, Vector3.right - Vector3.forward,  -Vector3.right - Vector3.forward, Vector3.forward-
            Vector3.right };
    void LateUpdate()
    {
        if(_moveSystem.direction==Vector3.zero && _moveSystem.MovePaused && _moveSystem.OldDirection==Vector3.zero && rotate)//jeżeli inny ruch się nie wykonuje
        {
            _moveSystem.MovePaused = false;//zablokuj inny ruch
            switch (typeOfRotate)//obróć o daną rotacje
            {
                case RotateType.smallLeft:
                    for (int i = 0; i < _legsMovementToRotate.Length; i++)//każdą nogę
                    {
                        _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate[i] * _vecMultipliyer);//obróć o daną pozycje
                    }
                    _rotateHead.Rotate(-_halfQouaterCircle, _moveSystem.animClat,_moveSystem.speedAn);//obróć głowę z tułowiem
                    ChangeRotationNumber(1);//zmień numer rotacji
                    break;
                case RotateType.BigLeft:
                    if(_posNumber%2==0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate[i] * _vecMultipliyer*2);
                        }
                        _rotateHead.Rotate(-_halfQouaterCircle*2, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(2);
                    }
                    else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i), _legsMovementToRotate2[i] * _vecMultipliyer);
                        }
                        _rotateHead.Rotate(-_halfQouaterCircle*2, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(2);
                    }
                    break;
                case RotateType.smallright:
                    for (int i = 0; i < _legsMovementToRotate.Length; i++)
                    {
                        _moveSystem.MoveForOneLeg(NumOfLeg(NumOfDirInf(i, 1), false), _legsMovementToRotate[i] * _vecMultipliyer);
                    }
                    _rotateHead.Rotate(_halfQouaterCircle, _moveSystem.animClat, _moveSystem.speedAn);
                    ChangeRotationNumber(-1);
                    break;
                case RotateType.BigRight:
                    if (_posNumber % 2 == 0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(NumOfDirInf(i, 1), false), _legsMovementToRotate[i] * _vecMultipliyer*2);
                        }
                        _rotateHead.Rotate(_halfQouaterCircle*2, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(-2);
                    }
                    else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate2[i] * _vecMultipliyer);
                        }
                        _rotateHead.Rotate(_halfQouaterCircle*2, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(-2);
                    }
                        break;
                case RotateType.fullHalf:
                    if (_posNumber % 2 == 0)
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate2[i] * _vecMultipliyer * 2);
                        }

                        _rotateHead.Rotate(_halfQouaterCircle*4, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(-4);
                    }
                    else
                    {
                        for (int i = 0; i < _legsMovementToRotate.Length; i++)
                        {
                            _moveSystem.MoveForOneLeg(NumOfLeg(i, false), _legsMovementToRotate[i] * _vecMultipliyer * 2);
                        }
                        _rotateHead.Rotate(_halfQouaterCircle*4, _moveSystem.animClat, _moveSystem.speedAn);
                        ChangeRotationNumber(-4);
                    }
                    break;
                default:
                    break;
            }
            
                rotate = false;
        }
        
    }
    private void ChangeRotationNumber(int i)//zmień numer obecnej rotacji w jakiej znajduje się pająk
    {
        _posNumber += i;
        _posNumber = _posNumber < 0 ? _legsPosPosition + _posNumber : _posNumber;
        _posNumber = _posNumber > _legsPosPosition - 1 ? _posNumber - _legsPosPosition : _posNumber;
    }
    private int NumOfLeg(int i, bool plus=true)//zwraca numer nogi, jeżeli index wyjedzie poza to liczy od pierwszej nogi (zapętla od 0-3)
    {
        if (plus)
        {
            return (_posNumber / 2) + i > 3 ? (_posNumber / 2) + i - 4 : (_posNumber / 2) + i;
        }else
        {
            return ((_posNumber + 1) / 2) + i > 3 ? ((_posNumber + 1) / 2) + i - 4 : ((_posNumber + 1) / 2) + i;
        }
    }
    private int NumOfDirInf(int i,int dif)//przesunięcie
    {
        return (i+ dif) % _legsMovementToRotate.Length;
    }
}
