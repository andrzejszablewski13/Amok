﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;

namespace DoomWave
{
    [RequireComponent(typeof(SpiderScripts))]
    [RequireComponent(typeof(LineRenderer))]
    public class SpiderAI : BaseEnemy
    {
        private RaycastHit hit;
        private bool _leftEye;
        private SpiderScripts _scripts;
        private LineRenderer _lineRender;
        private float _speed=3, _decSpeed = 1;
        private int _halfQouaterCircle = 45;
        
        public override void Ready()
        {
            base.Ready();
            _scripts = this.GetComponent<SpiderScripts>();
            _detColl.myDetPlayer -= DoDmgMeele;
            _aIControler.enabled = false;
            _lineRender = this.GetComponent<LineRenderer>();
            _timeToResetDest = _decSpeed;
            _scripts._movScript.speedAn = _speed;
        }
        protected override void OnDisable()
        {
            if (_ready)
            {
                base.OnDisable();
            }
        }
        private float _rotation;
        private Vector3 _oldPlayerPos;
        public override void DoAction()
        {
            _lineRender.positionCount = 0;
            if (_playerInSide)
            {
                
                _rotation = Vector3.SignedAngle(_player.transform.forward, _scripts.eyeL.transform.parent.position - _player.transform.position,Vector3.up);
                if (Mathf.Abs(_rotation)< _halfQouaterCircle)
                {
                    if (_oldPlayerPos != Vector3.zero)
                    {
                        Shot();
                    }
                    else
                    {
                        _oldPlayerPos = _player.transform.position;
                    }
                }else
                {
                    Debug.Log(_rotation);
                    if (Compare(-_halfQouaterCircle))
                    {
                        _scripts._rotateScript.typeOfRotate = ProcBezierRotate.RotateType.smallLeft;
                        _scripts._rotateScript.rotate = true;
                    }
                    else if (!Compare(_halfQouaterCircle))
                    {
                        _scripts._rotateScript.typeOfRotate = ProcBezierRotate.RotateType.smallright;
                        _scripts._rotateScript.rotate = true;
                    }
                }
            }
        }
        private bool Compare(int axis)
        {
            return _rotation< axis ? true : false;
         
        }
        private void Shot()
        {
            if(Physics.Raycast(_leftEye?_scripts.eyeL.transform.position:_scripts.eyeR.transform.position, _oldPlayerPos, out hit))
            {
                _lineRender.positionCount = 2;
                _lineRender.SetPosition(0, _leftEye ? _scripts.eyeL.transform.position : _scripts.eyeR.transform.position);
                _lineRender.SetPosition(0, hit.point);
                if(hit.collider.TryGetComponent(out IHitByRange hited))
                {
                    hited.TakeDmg(dmg);
                }
                _oldPlayerPos = Vector3.zero;
            }
        }
        protected override void DoDmgMeele(IHitByMeele hittedPlayer)
        {
        //nothing-no meele dmg
            
        }
    }
}