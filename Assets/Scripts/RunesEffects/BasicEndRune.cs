﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomEffects
{
    public class BasicEndRune : RuneConnector
    {
        // Start is called before the first frame update
	    private Rigidbody _rbCrystal;
	    private int _dmgRecailModyficator=5;
        private float _scatterMod;
        private float _horScatter, _verScatter;
        protected override void Start()
        {
            base.Start();
            _rbCrystal = this.transform.GetChild(0).GetChild(0).GetComponent<Rigidbody>();
        }
        protected override void OnEnable()
        {
            base.OnEnable();
        }
        protected override void OnDisable()
        {
            base.OnDisable();
        }
        private void Update()
        {
            if (base._fire)
            {
                LoadMana();
                FireBullet();
            }
            else
            {
            }
        }
        private void FireBullet()
        {
            _loadingTime += Time.deltaTime;
            if (this.gameObject.activeInHierarchy && _loadingTime >= _controler._actualData.runes[_runeID].loadingManaTime && this._manaStoraged >= _controler._actualData.runes[_runeID].manaChange )
            {
                _scatterMod = _dmgRecailModyficator * _previusRuneConnector._tempDmg;
                _loadingTime = 0;
                _horScatter = _scatterMod * (_controler._actualData.runes[_runeID].wScatter.x+_tempWScatter.x);
                _horScatter = _horScatter > 0 ? _horScatter : 0;
                _verScatter = _scatterMod * (_controler._actualData.runes[_runeID].wScatter.y+_tempWScatter.y);
                _verScatter = _verScatter > 0 ? _verScatter : 0;
                _controler.SetBullet(_previusRuneConnector._tempDmg, _previusRuneConnector._tempSpeed, this.gameObject.transform, 
                    _previusRuneConnector._tempKnockBack, _horScatter, _verScatter);
	            _rbCrystal.AddForce(-this.transform.forward * _scatterMod, ForceMode.Impulse);
                this._manaStoraged -= Mathf.RoundToInt(_controler._actualData.runes[_runeID].manaCost);
            }
        }
    }
}