﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomEffects
{ 
public class ManaRecyclerRune : RuneConnector
    {
        // Start is called before the first frame update


        protected override void Start()
        {
            base.Start();
        }
        protected override void OnEnable()
        {
            base.OnEnable();
        }
        protected override void OnDisable()
        {
            base.OnDisable();
        }
        private void Update()
        {
            if (base._fire || base._load)
            {
                ActivateLaser();
                LoadMana();
                ChangeLaser();
            }
            else
            {
                DeactivateLaser();
            }
        }
        private float _manaStack, _maxManaStack;
        private Vector3 _rotate;
        private void ChangeLaser()
        {
	        _manaStack = this._manaStoraged;
            _maxManaStack = _controler._actualData.runes[_runeID].maxManaStorage;
            _rotate = Vector3.one * Mathf.InverseLerp(0, _maxManaStack, _manaStack) * 10;
            for (int i = 0; i < _circlesOfCrystals[0].childCount; i++)
            {
                _circlesOfCrystals[0].GetChild(i).GetComponent<Rigidbody>().angularVelocity = _rotate;
            }
            _tempDmg = _previusRuneConnector._tempDmg * _controler._actualData.runes[_runeID].dmgChange;
	        _tempSpeed = _previusRuneConnector._tempSpeed+_controler._actualData.runes[_runeID].speedOfBullet;
	        _tempKnockBack = _previusRuneConnector._tempKnockBack+_controler._actualData.runes[_runeID].knockBack;
	        _tempWScatter=_previusRuneConnector._tempWScatter+_controler._actualData.runes[_runeID].wScatter;

        }
        private void ActivateLaser()
        {

            _ = _lasers[0, 0].enabled == false && (_lasers[0, 0].enabled = true);
            _lasers[0, 0].SetPosition(0, _circlesOfCrystals[0].GetChild(0).localPosition);
            _lasers[0, 0].SetPosition(1, _lasers[0, 0].transform.InverseTransformPoint(nextRunes[0].transform.GetChild(0).GetChild(0).position));

        }
        private void DeactivateLaser()
        {
            if (_lasers[0, 0].enabled == true)
            {
                _ = _lasers[0, 0].enabled == true && (_lasers[0, 0].enabled = false);
                for (int i = 0; i < _circlesOfCrystals[0].childCount; i++)
                {
                    _circlesOfCrystals[0].GetChild(i).GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                }
            }

        }
    }
}