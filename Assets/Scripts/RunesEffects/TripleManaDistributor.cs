﻿
using UnityEngine;

namespace DoomEffects
{ 
public class TripleManaDistributor: RuneConnector
    {
        // vase


        protected override void Start()
        {
            base.Start();
        }
        protected override void OnEnable()
        {
            base.OnEnable();
        }
        protected override void OnDisable()
        {
            base.OnDisable();
        }
        private void Update()
        {
            if (base._fire || base._load)
            {
                ActivateLaser();
                LoadMana();
                ChangeLaser();
            }
            else
            {
                DeactivateLaser();
            }
        }
        private float _manaStack, _maxManaStack;
        private Vector3 _rotate;
        private void ChangeLaser()
        {
            _manaStack = this._manaStoraged;
            _maxManaStack = _controler._actualData.runes[_runeID].maxManaStorage;
            
		//place for visual effects like rotation

            _tempDmg = _previusRuneConnector._tempDmg * _controler._actualData.runes[_runeID].dmgChange;
	        _tempSpeed = _previusRuneConnector._tempSpeed+_controler._actualData.runes[_runeID].speedOfBullet;
	        _tempKnockBack = _previusRuneConnector._tempKnockBack+_controler._actualData.runes[_runeID].knockBack;
	        _tempWScatter=_previusRuneConnector._tempWScatter+_controler._actualData.runes[_runeID].wScatter;

        }
        private void ActivateLaser()
        {
            //place for activating lasers
            for (int i = 0; i < _circlesOfLasers[0].childCount; i++)
            {
                _ = _lasers[0, i].enabled == false && (_lasers[0, i].enabled = true);
                _lasers[0, i].SetPosition(1, _circlesOfCrystals[1].GetChild(i).localPosition + _circlesOfCrystals[1].localPosition);
                _lasers[0, i].SetPosition(0, _circlesOfCrystals[0].GetChild(0).localPosition);
            }
            for (int i = 0; i < _circlesOfLasers[1].childCount; i++)
            {
                if (nextRunes[i] != null)
                {
                    _ = _lasers[1, i].enabled == false && (_lasers[1, i].enabled = true);
                    _lasers[1, i].SetPosition(0, _circlesOfCrystals[1].GetChild(i).localPosition);
                    _lasers[1, i].SetPosition(1, _lasers[1, i].transform.InverseTransformPoint(nextRunes[i].transform.GetChild(0).GetChild(0).position));
                }
            }

        }
        private void DeactivateLaser()
        {
            //place for deactivating lasers

            if (_lasers[0, 0].enabled == true)
            {
                for (int i = 0; i < _circlesOfLasers[0].childCount; i++)
                {
                    _ = _lasers[0, i].enabled == true && (_lasers[0, i].enabled = false);
                }

                for (int i = 0; i < _circlesOfLasers[1].childCount; i++)
                {
                    _ = _lasers[1, i].enabled == true && (_lasers[1, i].enabled = false);
                }
            }

            }
    }
}