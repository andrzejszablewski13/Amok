﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomEffects
{
    public class RuneConnector : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject previusRune;
        public GameObject[] nextRunes;
        protected RuneConnector _previusRuneConnector;
        [SerializeField] protected DoomWave.NamesOfModuls _nameOfElement;
        protected Transform[] _circlesOfCrystals, _circlesOfLasers,_circleOfJoints;
        protected LineRenderer[,] _lasers;
        protected Rigidbody[,] _rB;
        protected bool _active;
        protected DoomWave.RuneControler _controler;
        protected bool _fire = false,_load=false;
        [HideInInspector] public int _runeID,_previusRuneID;
        protected float _loadingTime = 0;
        public float _manaStoraged = 0;
        private Vector3[] _orPosOfJoints;
        [HideInInspector] public float _tempDmg, _tempSpeed, _tempKnockBack;
        [HideInInspector] public Vector2 _tempWScatter;

        protected virtual void Start()
        {
            GetReferences();
        }
        public void GetReferences()
        {
            _controler = this.transform.GetComponentInParent<DoomWave.RuneControler>();
            
            for (int i = 0; i < _controler._actualData.runes.Length; i++)
            {
                
                if (_controler._actualData.runes[i].name==_nameOfElement)
                {
                    _runeID = i;
                }
            }
            if (previusRune != null)
            {
                _previusRuneConnector = previusRune.GetComponent<RuneConnector>();
                _previusRuneID = _previusRuneConnector._runeID;
            }
            _controler.firing += StartFire;
            _circlesOfCrystals = new Transform[this.transform.GetChild(0).childCount];
            _circlesOfLasers = new Transform[this.transform.GetChild(1).childCount];
            _circleOfJoints = new Transform[this.transform.GetChild(2).childCount];
            _lasers = new LineRenderer[_circlesOfLasers.Length, 4];
            for (int i = 0; i < this.transform.GetChild(0).childCount; i++)
            {
                _circlesOfCrystals[i] = this.transform.GetChild(0).GetChild(i);
            }
            for (int i = 0; i < this.transform.GetChild(1).childCount; i++)
            {
                _circlesOfLasers[i] = this.transform.GetChild(1).GetChild(i);
                for (int j = 0; j < _circlesOfLasers[i].childCount; j++)
                {
                    _lasers[i, j] = _circlesOfLasers[i].GetChild(j).GetComponent<LineRenderer>();
                }
            }
            _orPosOfJoints = null;
            _orPosOfJoints = new Vector3[_circleOfJoints.Length];
            for (int i = 0; i < this.transform.GetChild(2).childCount; i++)
            {
                _circleOfJoints[i] = this.transform.GetChild(2).GetChild(i);
                _orPosOfJoints[i] = _circleOfJoints[i].transform.localPosition;
            }

        }
        public void HideCrystals(bool yesno,Vector3 position)
        {
            if(yesno)
            {
                for (int i = 0; i < _circleOfJoints.Length; i++)
                {
                    _circleOfJoints[i].transform.localPosition = this.transform.InverseTransformPoint(position);
                }
            }else
            {
                for (int i = 0; i < _circleOfJoints.Length; i++)
                {
                    _circleOfJoints[i].transform.localPosition = _orPosOfJoints[i];
                }
            }
        }

        protected virtual void OnEnable()
        {
            if(_controler!=null)
            {
                _controler.firing += StartFire;
                _controler.preLoadingWeapon += StartLoading;
            }
        }
        protected virtual void OnDisable()
        {
            if (_controler != null)
            {
                _controler.firing -= StartFire;
                _controler.preLoadingWeapon -= StartLoading;
                _load = false;
                _fire = false;
            }
        }
        protected void StartLoading(bool yesNo)
        {
            _load = yesNo;
            if(_fire)
            {
                _load = false;
            }
        }
        protected void StartFire(bool yesNo)
        {
            _fire = yesNo;
            _load = false;
        }
        protected void LoadMana()
        {
            _loadingTime += Time.deltaTime;
            if (_loadingTime >= _controler._actualData.runes[_runeID].loadingManaTime && this._manaStoraged <= _controler._actualData.runes[_runeID].maxManaStorage)
            {
                _loadingTime = 0;
                if (previusRune == null)
                {
                    if (_controler._player.data.manaStack > _controler._actualData.runes[_runeID].manaCost / _controler._actualData.runes[_runeID].manaChange )
                    {
                        this._manaStoraged += _controler._actualData.runes[_runeID].manaCost * _controler._actualData.runes[_runeID].manaChange;
                        _controler._player.data.manaStack -= Mathf.RoundToInt(_controler._actualData.runes[_runeID].manaCost / _controler._actualData.runes[_runeID].manaChange);
                    }
                }
                else
                {
                    
                    if (_previusRuneConnector._manaStoraged > _controler._actualData.runes[_runeID].manaCost / _controler._actualData.runes[_runeID].manaChange)
                    {
                       // Debug.Log("test2");
                        this._manaStoraged += _controler._actualData.runes[_runeID].manaCost * _controler._actualData.runes[_runeID].manaChange;
                        _previusRuneConnector._manaStoraged -= _controler._actualData.runes[_runeID].manaCost / _controler._actualData.runes[_runeID].manaChange;

                    }
                    //Debug.Log(_previusRuneConnector._manaStoraged+" "+ _controler._actualData.runes[_runeID].manaCost / _controler._actualData.runes[_runeID].manaChange);
                }
            }
        }
    }
}