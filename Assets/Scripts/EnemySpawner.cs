﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class EnemySpawner : MonoBehaviour
    {
        private Transform _tempTransform;
        [SerializeField] ModifiersData _modsData;
        [SerializeField] BaseEnemiesStat _statsData;
        private float _temp,_temp2;
        private int _trescholdSpiderSpawn = 23, _trescholdBatSpawn = 3,_spiderBonusValue=4,_batBonusValue=1,_chanceForBatSpawn=3;
        private enum Enemies
        {
	        TestEnemy,slugblend,Spider,legMonster_final,bat_final
        }
        private int _testRandom;
        // Start is called before the first frame update
        public void BaseEnemySpawn(RoomControler _room)
        {
            for (int i = 0; i < (int)_modsData.lineralData.enemyNumber* _modsData.geometricData.enemyNumber; i++)
            {
	            if ((int)_modsData.lineralData.enemyNumber * _modsData.geometricData.enemyNumber - i >= _trescholdSpiderSpawn)//jeżeli liczbe przeciwników odpowiednio wysoka spawn pająki
                {
                    SpawnSpider(_room);
                    i += _spiderBonusValue;
                }
                else if((int)_modsData.lineralData.enemyNumber * _modsData.geometricData.enemyNumber - i >= _trescholdBatSpawn)//jeżeli liczbe przeciwników odpowiednio wysoka spawn nietoperze
                {
                    _testRandom = Random.Range(1, _chanceForBatSpawn);
                    if (_testRandom == 2)
                    {
                        BatSpawner(_room);
                        i += _batBonusValue;
                    }
                    else
                    {
                        SpawnSlugBend(_room);
                    }
                }else
                {
                    SpawnSlugBend(_room);//jak nie to podstawowe
                }
            }
        }
        private void BatSpawner(RoomControler _room)//spawn pająka w odpowiedni sposób
        {
            _tempTransform = PoolManager.Pools[PopularStrings.Spawner.ToString()].Spawn(Enemies.bat_final.ToString(),
                  new Vector3(_room.flor.transform.position.x, 3, _room.flor.transform.position.z + _room.flor.transform.localScale.z / 2),
                   Quaternion.identity,
                   _room.enemies.transform);
	        _tempTransform.localPosition = new Vector3(Random.Range(-1 * (_room.flor.transform.localScale.x / 2 - 2), (_room.flor.transform.localScale.x / 2 - 1))
		        , 2
		        , Random.Range(1, (_room.flor.transform.localScale.z - 2)));
            SetStats(_statsData._bat, _tempTransform.GetComponent<BaseEnemy>());
        }
        private void SpawnSpider(RoomControler _room)//spawn bata w odpowiedni sposób
        {
            _tempTransform = PoolManager.Pools[PopularStrings.Spawner.ToString()].Spawn(Enemies.Spider.ToString(),
                  new Vector3(_room.flor.transform.position.x, -1, _room.flor.transform.position.z + _room.flor.transform.localScale.z / 2),
                   Quaternion.identity,
                   _room.enemies.transform);
            _tempTransform.localPosition = new Vector3(Random.Range(-1 * (_room.flor.transform.localScale.x / 2 - 1), (_room.flor.transform.localScale.x / 2 - 1))
                , -1
                , Random.Range(1, (_room.flor.transform.localScale.z - 1)));
            SetStats(_statsData._spider, _tempTransform.GetComponent<BaseEnemy>());
        }
        private void SpawnSlugBend(RoomControler _room)//spawn sluga w odpowiedni sposób
        {
            _tempTransform = PoolManager.Pools[PopularStrings.Spawner.ToString()].Spawn(Enemies.slugblend.ToString(),
                   new Vector3(_room.flor.transform.position.x, 1, _room.flor.transform.position.z + _room.flor.transform.localScale.z / 2),
                    Quaternion.identity,
                    _room.enemies.transform);
            _tempTransform.localPosition = new Vector3(Random.Range(-1 * (_room.flor.transform.localScale.x / 2 - 1), (_room.flor.transform.localScale.x / 2 - 1))
                , 1
                , Random.Range(1, (_room.flor.transform.localScale.z - 1)));
            //set variables for enemy randomly from range gratened by elite lvl
            SetStats(_statsData._testEnemy, _tempTransform.GetComponent<BaseEnemy>());
        }
        private void SetStats(BaseStatOfSingleEnemy stat,BaseEnemy enemyAIClass)//przydziel spawnojucemu przeciwnikowi dane
        {
            _temp = (stat.speedgrow * _modsData.lineralData.enemySpeed + stat.speed) * _modsData.geometricData.enemySpeed;
            _temp2 = (_modsData.lineralData.enemyElite * _modsData.lineralData.enemySpeed + _temp * 2) * _modsData.geometricData.enemyElite;
            enemyAIClass.speed = (int)Random.Range(_temp / 2, _temp2);
            _temp = (stat.dmggrow * _modsData.lineralData.enemyDmg + stat.dmg) * _modsData.geometricData.enemyDmg;
            _temp2 = (_modsData.lineralData.enemyElite * _modsData.lineralData.enemyDmg + _temp * 2) * _modsData.geometricData.enemyElite;
            enemyAIClass.dmg = (int)Random.Range(_temp / 2, _temp2);
            _temp = (stat.hpgrow * _modsData.lineralData.enemyHp + stat.hp) * _modsData.geometricData.enemyHp;
            _temp2 = (_modsData.lineralData.enemyElite * _modsData.lineralData.enemyHp + _temp * 2) * _modsData.geometricData.enemyElite;
            enemyAIClass.hP = (int)Random.Range(_temp / 2, _temp2);
            StartCoroutine(LaterStart(enemyAIClass));
        }
        IEnumerator LaterStart(BaseEnemy objectToActive)//jak gotowy to uruchom
        {
            yield return new WaitForFixedUpdate();
            objectToActive.gameObject.SetActive(true);
            objectToActive.Ready();
        }
    }
}