﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
	private Button _button;
	[Serializable] enum Sceens
	{
		Menu,Level
	}
	[SerializeField] private Sceens _sceneToLoad;
// Start is called before the first frame update

	private void Start()
	{
		if(	this.TryGetComponent<Button>(out _button))
		{_button.onClick.RemoveAllListeners();
			_button.onClick.AddListener(ChangeSceneNow);
		}
	}
// Update is called once per frame
	public void ChangeSceneNow()
	{
		SceneManager.LoadScene(_sceneToLoad.ToString());
	}
}