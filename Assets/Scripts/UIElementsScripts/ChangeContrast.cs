﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class ChangeContrast : MonoBehaviour
{
    [System.Serializable]
   public  enum ColorChange
    {
        Contrast,Shift,Saturation
    }
    // Start is called before the first frame update
    [SerializeField] private Volume _vol;
    private ColorAdjustments _temp;
    private Slider _slider;
    void Start()
    {
        _slider = this.GetComponent<Slider>();
        _vol.profile.TryGet<ColorAdjustments>(out _temp);
    }
    public void OnChange(int index)
    {
        // _vol.sharedProfile.components[0].parameters[1].SetValue(VolumeParameter);

            if (_temp!=null)
            {
            switch (index)
            {
                case 0:
                    _temp.contrast.value = _slider.value;
                    break;
                case 1:
                    _temp.hueShift.value = _slider.value;
                    break;
                case 2:
                    _temp.saturation.value = _slider.value;
                    break;
                default:
                    break;
            }
            }

    }

}
