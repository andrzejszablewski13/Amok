﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class ChangeCanvas : MonoBehaviour
{
	// Start is called before the first frame update
	private Button _button;
	[Serializable] enum CanvasGroups
	
	{
		Menu,Options_PactRules,Obituary_Credits,Graphic_Menu,Audio_Menu,Controls_Menu,Escape_Exit 
	}
	[SerializeField] private CanvasGroups  _newCanvas;
	[SerializeField] private GameObject _canvas, _thisGroup;
	private void Start()
	{
		if(	this.TryGetComponent<Button>(out _button))
		{_button.onClick.RemoveAllListeners();
			_button.onClick.AddListener(ChangeNow);
		}
	}
	public void ChangeNow()
	{
		for (int i = 0; i < _canvas.transform.childCount; i++) 
		{
			if(_canvas.transform.GetChild(i).name==_newCanvas.ToString())
			{
				_canvas.transform.GetChild(i).gameObject.SetActive(true);
				_thisGroup.SetActive(false);
				break;
				
			}
		}
	}

}