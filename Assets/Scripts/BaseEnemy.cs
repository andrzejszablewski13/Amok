﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DoomWave
{
	//base for enemy controler-for nov is only for prefab enemy
    [RequireComponent(typeof(DetectCollisionWitchPalyer))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class BaseEnemy : MonoBehaviour,IHitByRange,IHitByMeele
    {
        protected RoomControler _roomControler;
        protected NavMeshAgent _aIControler;
        public bool _playerInSide = false;
        public int hP = 10;
        protected float _timeToResetDest =0.2f;
        public int dmg = 5,speed=5;
        protected DetectCollisionWitchPalyer _detColl;
        protected GameObject _player;
        protected bool _ready = false;

        protected void Start()
	    {
		    //if not spawned ready on start
            if (!PoolManager.Pools[PopularStrings.Spawner.ToString()].IsSpawned(this.transform))
                Ready();
        }
        protected void OnPlayerInside()
	    {
		    //set if player enter a room
            _playerInSide = true;
        }
        public virtual void Ready()
        {
            _ready = true;
            _player = PrimalClass.Singleton.GetPlayer();
            _detColl = this.GetComponent<DetectCollisionWitchPalyer>();
            _roomControler = this.transform.parent.parent.GetComponent<RoomControler>();
            _aIControler = this.transform.GetComponent<NavMeshAgent>();
            _aIControler.speed = speed;
            _roomControler.PlayerInside += OnPlayerInside;
            _detColl.myDetPlayer += DoDmgMeele;
            StartCoroutine(FollowPlayer());
        }
	    protected virtual void OnDisable()
	    {
            if (_ready)
            {
                _roomControler.PlayerInside -= OnPlayerInside;
                _detColl.myDetPlayer -= DoDmgMeele;
                _playerInSide = false;
                StopAllCoroutines();
            }
	    }
        //if player inside follow him
        protected IEnumerator FollowPlayer()
        {
            while (true)
            {
                if (_playerInSide)
                {
                    DoAction();

                }
                

                yield return new WaitForSeconds(_timeToResetDest);
            }
        }
        public virtual void DoAction()
        {
            _aIControler.SetDestination(_player.transform.position);
        }
        public void TakeDmg(int value)
        {
            hP -= value;
            if(hP<=0)
            {
                this.transform.parent = this.transform.parent.parent;
                if (PoolManager.Pools[PopularStrings.Spawner.ToString()].IsSpawned(this.transform))
                {
                    PoolManager.Pools[PopularStrings.Spawner.ToString()].Despawn(this.transform);
                }
                else
                {
                    Destroy(this.gameObject);
                }
                _roomControler.enemyDie();


            }
        }

        public void TakeDmgClose(int value)
        {
            hP -= value;
            if (hP <= 0)
            {
                this.transform.parent = this.transform.parent.parent;
                if (PoolManager.Pools[PopularStrings.Spawner.ToString()].IsSpawned(this.transform))
                {
                    PoolManager.Pools[PopularStrings.Spawner.ToString()].Despawn(this.transform);
                }
                else
                {
                    Destroy(this.gameObject);
                }
                _roomControler.enemyDie();

            }
        }
        //do dmg to player by meele

        protected virtual void DoDmgMeele(IHitByMeele hittedPlayer)
        {

                hittedPlayer.TakeDmgClose(dmg);
            
        }

    }
}