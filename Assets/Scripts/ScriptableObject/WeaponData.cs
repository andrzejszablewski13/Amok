﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
   
    [Serializable]
    public struct RuneDataStack
    {
        public NamesOfModuls name;
        public float dmgChange;
        public RuneType model;
        public float speedOfBullet, loadingManaTime,manaCost,manaStorage,maxManaStorage,manaChange,knockBack;
        public float modOfDmg, modOfRecail, modBulletSpeed,modOfManaStorage,modOfManaCharge,modOfManaCost,modKnockBack;
	    public int lenght;
	    public Vector2 wScatter;
	    public Vector2 wScatterMod;
	    public GameObject _preFab;
	    public string descriptions;
    }

    [CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData")]
    public class WeaponData : ScriptableObject
    {
        public RuneDataStack[] runes;
    }

}