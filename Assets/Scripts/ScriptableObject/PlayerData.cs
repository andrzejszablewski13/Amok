﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    [Serializable]
    public struct PlayerDataStruct
    {
        public string name;
        public int hp, maxHp, maxNumberJump, maxNumberDasch,jumpNumber,DaschNumber;
        public float maxSpeed, jumpPower, daschPower, mouseSensivity,maxYAngle;
        public int manaStack,maxManaStack, dmg;
        public float rangeCalcInTime;
    }
    [CreateAssetMenu(fileName = "Player1Data", menuName = "ScriptableObjects/PlayerData")]
    public class PlayerData : ScriptableObject
    {
        public PlayerDataStruct data;
    }
}