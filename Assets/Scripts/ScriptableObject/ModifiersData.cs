﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    [Serializable]
    public struct ModufighersDataStruct
    {
        public float  playerSpeed;
        public float playerJumpPower,playerDaschPower;
        public float enemySpeed, enemyDmg, enemyNumber, enemyElite, enemyHp;
        public float playerMaxHp,playerMaxMana;
    }
    [CreateAssetMenu(fileName = "Modifiers", menuName = "ScriptableObjects/Modifiers")]
    public class ModifiersData : ScriptableObject
    {
        public ModufighersDataStruct lineralData, geometricData;
    }
}