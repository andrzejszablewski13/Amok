﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DoomWave
{
    [Serializable]
    public struct BaseStatOfSingleEnemy
    {
        public int dmg, speed,hp;
        public int dmggrow, speedgrow, hpgrow;
    }
    [CreateAssetMenu(fileName = "EnemiesData", menuName = "ScriptableObjects/EnemiesData")]
    public class BaseEnemiesStat : ScriptableObject
    {
        // Start is called before the first frame update
	    public BaseStatOfSingleEnemy _testEnemy,_spider,_bat;
    }
}