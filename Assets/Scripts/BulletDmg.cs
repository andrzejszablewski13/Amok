﻿using PathologicalGames;
using System.Collections;
using UnityEngine;

namespace DoomWave
{
    public class BulletDmg : MonoBehaviour
    {
        // Start is called before the first frame update
        public int dmg = 10;
        public float knockBack=10;
        private float _timeToDie;
        private IEnumerator _corutin;
        private Rigidbody _rB;
        public float TimeToDie
        {
            get { return _timeToDie; }
            set { _timeToDie = value;
                _corutin = DespawnAfterTime(value);
                StartCoroutine(_corutin);
            }
        }
	    //do dmg to objects if possible by range
        private IHitByRange _temp;
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.TryGetComponent(out  _temp))
            {
                _temp.TakeDmg(dmg);
                
                
            }
            if(collision.gameObject.TryGetComponent(out _rB))
            {
                _rB.AddForce(-collision.GetContact(0).normal * knockBack, ForceMode.Impulse);
            }
            if(PoolManager.Pools[PopularStrings.WeaponParts.ToString()].IsSpawned(this.transform))
                PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Despawn(this.transform);
        }
	    //despawn after some time if not hit anythink
        IEnumerator DespawnAfterTime(float value)
        {
            yield return new WaitForSeconds(value);
            if (PoolManager.Pools[PopularStrings.WeaponParts.ToString()].IsSpawned(this.transform))
                PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Despawn(this.transform);
        }
        public void OnDespawned()
        {
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            StopCoroutine(_corutin);
        }
    }
}