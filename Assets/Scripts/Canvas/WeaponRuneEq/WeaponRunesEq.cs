﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	//singleton for weapon forging
    public class WeaponRunesEq
    {
        private static WeaponRunesEq _singleton;
        private static Dictionary<NamesOfModuls, GenOnClick> _generators = new Dictionary<NamesOfModuls, GenOnClick>();
        private static Dictionary<int, RuneSlot> _runeSlotsControlers = new Dictionary<int, RuneSlot>();
        private static CheckIfPosToMerge _mergePosVis;
        private static GameObject _genRuneStorage;
        public static WeaponRunesEq Singleton
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new WeaponRunesEq();
                }
                return _singleton;
            }
            set
            {
                if (_singleton == null)
                {
                    _singleton = new WeaponRunesEq();
                }
                _singleton = value;
            }
        }
        public void AddGenRuneStorage(GameObject genrunestorage)
        {
            _genRuneStorage = genrunestorage;
        }
        public void AddVisOfPosWepMade(CheckIfPosToMerge merging)
        {
            _mergePosVis = merging;
        }
        public void AddGen(GenOnClick gen)
        {
            _generators.Add(gen.runetype, gen);
        }
        public GenOnClick GetGenerator(NamesOfModuls neededModul)
        {
            return _generators[neededModul];
        }
        public void AddRuneSlot(RuneSlot runeSlot)
        {
            _runeSlotsControlers.Add(int.Parse(runeSlot.gameObject.name), runeSlot);
        }
        public RuneSlot GetRuneSlot(string nameOfGO)
        {
            return _runeSlotsControlers[int.Parse(nameOfGO)];
        }
        public CheckIfPosToMerge GetVisForPosWepMade()
        {
            return _mergePosVis;
        }
        public GameObject GetGenRuneStorage()
        {
            return _genRuneStorage;
        }
    }
}