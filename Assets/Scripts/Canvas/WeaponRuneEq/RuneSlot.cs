﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DoomWave
{
	//get data from dragable modul visualization and atatch dragable visualization
    public class RuneSlot : MonoBehaviour,IDropHandler
    {
        [HideInInspector] public GameObject modul;
        [HideInInspector] public DragOptions dragControl;
        private NamesOfModuls _runetype;
        public delegate void ModulInSlotChange(NamesOfModuls value);
        public ModulInSlotChange ChangingModul;
        [HideInInspector] public NamesOfModuls Runetype
        {
            get { return _runetype; }
            set { 
                _runetype = value;
                ChangingModul(value);
                }        
        }
        private void Start()
        {
            WeaponRunesEq.Singleton.AddRuneSlot(this);
        }
        
        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                if(dragControl!=null)
                {
                    dragControl.DespawnThis();
                }
                modul = eventData.pointerDrag;

                dragControl = modul.GetComponent<DragOptions>();
                
                if(dragControl.attachedToGameObject == dragControl._generator.gameObject)
                {
                    dragControl._generator.SpawnModul();
                }else if (dragControl.attachedToGameObject != this.gameObject)
                {
                    WeaponRunesEq.Singleton.GetRuneSlot(dragControl.attachedToGameObject.name).ClearOldDataRuneFromThis();
               }
                dragControl.attachedToGameObject = this.gameObject; 
                    dragControl.inSlot = true;
                Runetype = dragControl.runetype;
                modul.transform.position = this.transform.position;
            }
        }
        public void ClearOldDataRuneFromThis()
        {
            Runetype = NamesOfModuls.empty;
            dragControl = null;
            modul = null;
        }
    }
}