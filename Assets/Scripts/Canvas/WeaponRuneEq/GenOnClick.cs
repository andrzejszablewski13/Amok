﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using PathologicalGames;

namespace DoomWave
{
	//gen new modul to drag on slot
    public class GenOnClick : MonoBehaviour
    {
        [SerializeField] private GameObject _preFab;
        public Sprite typeOfModul;
        public NamesOfModuls runetype;
        private Transform _temp;
        private DragOptions _tempDragOption;
        private int _runeID=-1;
        private WeaponData _data;
        private string _descrition;
        [SerializeField] private TMPro.TextMeshProUGUI _textDescriptionField;
        public void SetDescription()
        {
            _data = PrimalClass.Singleton.GetWEaponData();
            for (int i = 0; i < _data.runes.Length; i++)
            {
                if(_data.runes[i].name==runetype)
                {
                    _runeID = i;
                    break;
                }
            }
            _descrition = _data.runes[_runeID].descriptions;
        }
        public void SpawnModul()
        {
            if(_runeID==-1)
            {
                SetDescription();
            }
            _temp= PoolManager.Pools[PopularStrings.CanvasSpawnPool.ToString()].Spawn(_preFab,this.transform);
            _temp.position = this.transform.position;
            _temp.rotation = this.transform.rotation;
            _temp.GetComponent<UnityEngine.UI.Image>().sprite = typeOfModul;
            _tempDragOption = _temp.GetComponent<DragOptions>();
            _tempDragOption.runetype = runetype;
            _tempDragOption._generator = this;
            _tempDragOption.attachedToGameObject = this.gameObject;
            _tempDragOption.description = _descrition;
	        _tempDragOption.text = _textDescriptionField;
        }
        private void Start()
        {
            SpawnModul();
            WeaponRunesEq.Singleton.AddGen(this);
        }
    }
   

}