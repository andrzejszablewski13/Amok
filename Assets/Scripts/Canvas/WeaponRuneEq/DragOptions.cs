﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace DoomWave
{
	//drag system for moduls and show descriptions
    [RequireComponent(typeof(EventTrigger))]
    public class DragOptions : MonoBehaviour
    {

        private EventTrigger _trigger;
        private EventTrigger.Entry _onDragBegin, _onDrag, _onDragEnd, _onMousePointerEnter, _onMousePointerExit,_onDrop;
        public GenOnClick _generator;
        private CanvasGroup _canvasGroup;
        [HideInInspector] public NamesOfModuls runetype;
        [HideInInspector] public bool inSlot;
        [HideInInspector] public GameObject attachedToGameObject;
        [HideInInspector] public string description;
        [HideInInspector] public TextMeshProUGUI text;
        void Start()
        {
            _canvasGroup=this.gameObject.GetComponent<CanvasGroup>();
            _trigger = this.gameObject.GetComponent<EventTrigger>();
            _onDragBegin = new EventTrigger.Entry();
            _onDrag = new EventTrigger.Entry();
            _onDragEnd = new EventTrigger.Entry();
            _onMousePointerEnter = new EventTrigger.Entry();
            _onMousePointerExit = new EventTrigger.Entry();
            _onDrop = new EventTrigger.Entry();
            _onDragBegin.eventID = EventTriggerType.BeginDrag;
            _onDrag.eventID = EventTriggerType.Drag;
            _onDragEnd.eventID = EventTriggerType.EndDrag;
            _onMousePointerEnter.eventID = EventTriggerType.PointerEnter;
            _onMousePointerExit.eventID = EventTriggerType.PointerExit;
            _onDrop.eventID = EventTriggerType.Drop;
            _onDrag.callback.AddListener((data) => { OnDragging((PointerEventData)data); });
            _trigger.triggers.Add(_onDrag);
            _onDragEnd.callback.AddListener((data) => { OnDragEnd((PointerEventData)data); });
            _trigger.triggers.Add(_onDragEnd);
            _onDragBegin.callback.AddListener((data) => { OnDragBegin((PointerEventData)data); });
            _trigger.triggers.Add(_onDragBegin);
            _onMousePointerEnter.callback.AddListener((data) => { OnPointerEnter((PointerEventData)data); });
            _trigger.triggers.Add(_onMousePointerEnter);
            _onMousePointerExit.callback.AddListener((data) => { OnPointerExit((PointerEventData)data); });
            _trigger.triggers.Add(_onMousePointerExit);
            _onDrop.callback.AddListener((data) => { OnDrop((PointerEventData)data); });
            _trigger.triggers.Add(_onDrop);

        }
        private void OnPointerEnter(PointerEventData _)
        {
            text.text = description;
        }
        private void OnPointerExit(PointerEventData _)
        {
            text.text = "";
        }
        private void OnDragging(PointerEventData _)
        {
            transform.position = Mouse.current.position.ReadValue();
        }
        private void OnDragBegin(PointerEventData _)
        {
            _canvasGroup.blocksRaycasts = false;
            inSlot = false;
        }
        private void OnDragEnd(PointerEventData _)
        {
            _canvasGroup.blocksRaycasts = true;
            if(inSlot)
            {

            }else
            {
                DespawnThis();
            }
        }
        private DragOptions _tempDropComp;
        private void OnDrop(PointerEventData data)
        {
            if(data.pointerDrag.TryGetComponent(out _tempDropComp) && _tempDropComp!=this)
            {
                DespawnThis();
                this.attachedToGameObject.GetComponent<RuneSlot>().OnDrop(data);
            }
        }
        public void DespawnThis()
        {
            if (PoolManager.Pools[PopularStrings.CanvasSpawnPool.ToString()].IsSpawned(this.transform))
            {
                if (attachedToGameObject == WeaponRunesEq.Singleton.GetGenerator(runetype).gameObject)
                { 
                    WeaponRunesEq.Singleton.GetGenerator(runetype).SpawnModul(); 
                }
                else
                {
                    WeaponRunesEq.Singleton.GetRuneSlot(attachedToGameObject.name).ClearOldDataRuneFromThis();
                }

                PoolManager.Pools[PopularStrings.CanvasSpawnPool.ToString()].Despawn(this.transform);
            }
        }
 
    }
}