﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace DoomWave
{
    public class CheckIfPosToMerge : MonoBehaviour
    {
        private string _error = "Error";
        public enum CheckForError
        {
            Correct,NoEndFinded,EndNoConnectedToMain,EndAfterEnd,NoMainDetected
        }
        // Start is called before the first frame update
        [SerializeField] private GameObject _slotStorage;
        private RuneSlotNode[] _runesNodes;
        public CheckForError _errorOutput;
        private bool _endNotDetected = true;
        private TextMeshProUGUI _text;
        private void Start()
        {
            for (int i = 0; i < this.gameObject.transform.childCount; i++)
            {
                if(transform.GetChild(i).name.Contains(_error))
                {
                    _text = transform.GetChild(i).GetComponent<TextMeshProUGUI>();
                }
            }
            WeaponRunesEq.Singleton.AddVisOfPosWepMade(this);
        }
        public void CheckIfCanMadeWeapon(bool createNewWeapon=true)
        {
            _errorOutput = CheckForError.Correct;
            _text.text = "";
            _runesNodes = null;
            _runesNodes = new RuneSlotNode[_slotStorage.transform.childCount];
            _endNotDetected = true;
            for (int i = 0; i < _slotStorage.transform.childCount; i++)
            {
                _runesNodes[i] = _slotStorage.transform.GetChild(i).GetComponent<RuneSlotNode>();
            }
            for (int i = 0; i < _runesNodes.Length; i++)
            {
                if (_runesNodes[i].runeTypeName == RuneType.Main)
                {
                    break;
                }else if(i+1>= _runesNodes.Length)
                {
                    _errorOutput = CheckForError.NoMainDetected;
                    _text.text = CheckForError.NoMainDetected.ToString();
                    return;
                }
            }
            for (int i = 0; i < _runesNodes.Length; i++)
            {    
                if (_runesNodes[i].runeTypeName == RuneType.End)
                {
                    _errorOutput = RekuSearch(_runesNodes[i], true);
                    if (_errorOutput != CheckForError.Correct)
                    {
                        _text.text = _errorOutput.ToString();
                    }
                    _endNotDetected = false;
                }
                if (_runesNodes[i].runeTypeName == RuneType.Main)
                {
                      // RekuMainCol(_runesNodes[i]);
                }
            }
            if (_endNotDetected)
            {
                _errorOutput = CheckForError.NoEndFinded;
                _text.text = CheckForError.NoEndFinded.ToString();
                return;
            }

            if (_errorOutput==CheckForError.Correct && createNewWeapon)
            {
                this.gameObject.GetComponent<PrimalWeaponRebuild>().OnRebuildingWeapon(ref _runesNodes);
            }
        }
        private RuneSlotNode _tempRuneRef;
        private void RekuMainCol(RuneSlotNode runeSlot)
        {
            if(runeSlot.nextNode.Count==0)
            {
                return;
            }

                for (int i = 0; i < runeSlot.nextNode.Count; i++)
                {
                    _tempRuneRef = runeSlot.nextNode[i].GetComponent<RuneSlotNode>();
                
                    if (_tempRuneRef.runeTypeName == RuneType.Empty)
                    {
                        _tempRuneRef.lineRender.color = Color.yellow;
                    }
                    else if (_tempRuneRef.runeTypeName == RuneType.End)
                    {
                        _tempRuneRef.lineRender.color = Color.grey;
                    }
                    else if (_tempRuneRef != null)
                    {
                        _tempRuneRef.lineRender.color = Color.green;
                        RekuMainCol(_tempRuneRef);
                    }
                
                }

        }
        private CheckForError RekuSearch(RuneSlotNode runeSlot,bool starting=false)
        {
            runeSlot.lineRender.color = Color.green;
            if (runeSlot.previusNode.Count==0)
            {
                if(runeSlot.runeTypeName==RuneType.Main)
                {
                    return CheckForError.Correct;
                }
                else
                {
                    runeSlot.lineRender.color = Color.red;
                    return CheckForError.EndNoConnectedToMain;
                }
            }
            else if(starting)
            {
                    return RekuSearch(runeSlot.previusNode[0].GetComponent<RuneSlotNode>());
            }
            else
            {
                switch (runeSlot.runeTypeName)
                {
                    case RuneType.Empty:
                        runeSlot.lineRender.color = Color.red;
                        return CheckForError.EndNoConnectedToMain;
                        break;
                    case RuneType.End:
                        runeSlot.lineRender.color = Color.red;
                        return CheckForError.EndAfterEnd;
                        break;
                    default:
                        return RekuSearch(runeSlot.previusNode[0].GetComponent<RuneSlotNode>());
                        break;
                }
                
            }

        }

    }
    

}