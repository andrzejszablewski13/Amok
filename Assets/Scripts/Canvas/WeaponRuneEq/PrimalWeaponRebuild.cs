﻿using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DoomWave
{
    public class PrimalWeaponRebuild : MonoBehaviour
    {
        [SerializeField] private GameObject _weaponPlace;
        
        private RuneSlotNode[] _runesNodes;
        private Stack<int> _checkedNodes=new Stack<int>();
        private WeaponData _weapondata;
        private List<Transform> _listOfGenObjects = new List<Transform>();
        private Transform _tempToDestroy;
        // Start is called before the first frame update
        public void OnRebuildingWeapon(ref RuneSlotNode[] refSlotMode)
        {
            if (!_weaponPlace.GetComponent<RuneControler>()._readyToFire)
            {
                _weaponPlace.GetComponent<RuneControler>().HideWeapon(false);
            }

	        _weapondata = PrimalClass.Singleton.GetWEaponData();
	        //get runes slots
            _runesNodes = refSlotMode;
	        //delete old weapon
            if (_listOfGenObjects.Count > 0)
            {
                for (int i = _listOfGenObjects.Count - 1; i >= 0; i--)
                {
                    if (PoolManager.Pools[PopularStrings.WeaponParts.ToString()].IsSpawned(_listOfGenObjects[i]))
                    {
                        _listOfGenObjects[i].SetParent(null);
                        PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Despawn(_listOfGenObjects[i]);
                    }
                }
            }
            else
            {
                for (int i = _weaponPlace.transform.childCount - 1; i >= 0; i--)
                {
                    _tempToDestroy = _weaponPlace.transform.GetChild(i);
                    _tempToDestroy.SetParent(null);
                    //Debug.Log(i);
                    if (PoolManager.Pools[PopularStrings.WeaponParts.ToString()].IsSpawned(_tempToDestroy))
                    {
                        PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Despawn(_tempToDestroy);
                    }
                    else
                    {  
                        Destroy(_tempToDestroy.gameObject);
                    }

                }
            }
            _checkedNodes.Clear();
            //Debug.Log(_weaponPlace.transform.childCount + " " + _listOfGenObjects.Count);
            //Debug.Log(_weaponPlace.transform.childCount);
            _listOfGenObjects.Clear();
	        //made new
            StartCoroutine(WhaitForSureDestroy());
            
        }
        public void HideWeaponIfNeeded()
        {
            if (!_weaponPlace.GetComponent<RuneControler>()._readyToFire)
            {
                _weaponPlace.GetComponent<RuneControler>().HideWeapon(true);
            }
        }
        IEnumerator WhaitForSureDestroy()
	    {
		    //whait to make sure to clear old weapon data
            yield return new WaitForSecondsRealtime(0.5f);
            for (int i = 0; i < _runesNodes.Length; i++)
            {
                if (_runesNodes[i].runeTypeName == RuneType.End)
                {
                    RebuildingFromEnd(_runesNodes[i]);
                }
            }
            yield return new WaitForSecondsRealtime(0.2f);
            _weaponPlace.GetComponent<RuneControler>().DetectRunes();
            _runesNodes = null;
        }
        Vector3 pos = Vector3.zero;
        private Vector3 _toLowerDistanceBetModul = new Vector3(0.3f, 0, 0.5f);
        private Vector3 GenPosFromName(int _parseName,RuneSlotNode runeSlot)
	    {
		    //as name say gen pos of modul form slot name
            pos = Vector3.zero;
            pos.z = Mathf.Round(_parseName / Mathf.Pow(runeSlot._unit, runeSlot.gameObject.name.Length - 1))*_toLowerDistanceBetModul.z;
            pos.x = Mathf.Round((_parseName % Mathf.Pow(runeSlot._unit, runeSlot.gameObject.name.Length - 1)) / runeSlot._unit) == 1 ?
               ( -1 * _parseName % runeSlot._unit)*_toLowerDistanceBetModul.x : (_parseName % runeSlot._unit )* _toLowerDistanceBetModul.x;
            return pos;
        }
        private void SetConnections(Transform nextNode, Transform _tempNode)
	    {
		    //set connections to other moduls if needed
            nextNode.GetComponent<DoomEffects.RuneConnector>().previusRune = _tempNode.gameObject;
            nextNode.GetComponent<DoomEffects.RuneConnector>().GetReferences();

                _tempNode.GetComponent<DoomEffects.RuneConnector>().nextRunes[
                           (int)(_tempNode.GetComponent<DoomEffects.RuneConnector>().nextRunes.Length / 2 - (nextNode.localPosition.x * 1 / _toLowerDistanceBetModul.x))]
                                = nextNode.gameObject;
            
            _tempNode.GetComponent<DoomEffects.RuneConnector>().GetReferences();
        }
        private void RebuildingFromEnd(RuneSlotNode runeSlot,Transform nextNode=null)
        {
            int _parseName;

            _parseName = int.Parse(runeSlot.gameObject.name);
            //Debug.Log(_parseName);
            Transform _tempNode;
            Vector3 pos;
            pos = GenPosFromName(_parseName, runeSlot);
            if (!_checkedNodes.Contains(_parseName))
            {
                _checkedNodes.Push(_parseName);

	            //Debug.Log(_weapondata.runes[runeSlot.runeID]._preFab.name);
	            //gen new modul and set its pos and rotation
                _tempNode=PoolManager.Pools[PopularStrings.WeaponParts.ToString()].Spawn(_weapondata.runes[runeSlot.runeID]._preFab,_weaponPlace.transform);
                _tempNode.localPosition = pos;
                _tempNode.localEulerAngles = Vector3.zero;
                _tempNode.gameObject.SetActive(true);
                _listOfGenObjects.Add(_tempNode);

                if (nextNode!=null)
                {
                    
                    SetConnections(nextNode, _tempNode);
                }
                if(runeSlot.previusNode.Count>0)
                {
                    RebuildingFromEnd(runeSlot.previusNode[0].GetComponent<RuneSlotNode>(), _tempNode);
                }
            }else if(_weapondata.runes[runeSlot.runeID].model==RuneType.Distributor) //if needed to connect to alredy gen part of weapon
            {
                //tu kod by działał przy więcej niż jeden endzie to distrybutora
                //ma znaleść transform distributora

                pos = GenPosFromName(_parseName, runeSlot);
                for (int i = 0; i < _listOfGenObjects.Count; i++)
                {
                    if(_listOfGenObjects[i].localPosition==pos)
                    {
                        SetConnections(nextNode, _listOfGenObjects[i]);
                    }
                }
            }
        }

    }
}