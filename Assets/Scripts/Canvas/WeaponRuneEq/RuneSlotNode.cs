﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{

    [RequireComponent(typeof(RuneSlot))]
    public class RuneSlotNode : MonoBehaviour
    {
        private RuneSlot _slot;
        private WeaponData _weaponData;
        [HideInInspector] public int runeID;
        public int _conectionLenghtLeft;
        private string _tempString;
        [HideInInspector] public int _unit = 10;
        private Transform _parent;
        [HideInInspector] public List<GameObject> previusNode=new List<GameObject>();
        [HideInInspector] public List<GameObject> nextNode = new List<GameObject>();
	    [HideInInspector] public RuneType runeTypeName;
        [HideInInspector] public LineRendererHUD lineRender;
	    //set connctions value to active grid parts
        [HideInInspector] public int ConectionLenghtLeft
        {
            get { return _conectionLenghtLeft; }
            set
            {
                _conectionLenghtLeft = value >= 0 ? value : 0;
                if (int.Parse(this.gameObject.name) > 0)
                {
                    this.gameObject.SetActive(value > 0);
                }
                    _tempString = (int.Parse(this.gameObject.name) + Mathf.Pow(_unit, this.gameObject.name.Length-1)).ToString();
                if (_conectionLenghtLeft > 1)
                {
                    ActivDeactiveNextRune(_tempString,true);
                    
                }
                else
                {
                    ActivDeactiveNextRune(_tempString, false);
                }
            }
        }
        void Awake()
        {
            _parent = this.gameObject.transform.parent;
            _slot = this.gameObject.GetComponent<RuneSlot>();
            
            
        }
        private void Start()
        {
            if (int.Parse(this.gameObject.name) > 0)
            {
                this.gameObject.SetActive(false);
            }
            lineRender = this.gameObject.GetComponentInChildren<LineRendererHUD>();
        }
        private void OnEnable()
        {
            if(_slot==null)
            {
                _slot = this.gameObject.GetComponent<RuneSlot>();
            }
            _slot.ChangingModul += OnModulChange;
        }
        private void OnDisable()
        {
            if (_slot == null)
            {
                _slot = this.gameObject.GetComponent<RuneSlot>();
            }
            if (int.Parse(this.gameObject.name) > 0 && ConectionLenghtLeft==0 && _slot.dragControl!=null && _slot.dragControl.attachedToGameObject==this.gameObject)
            {
                _slot.dragControl.DespawnThis();
                _slot.ClearOldDataRuneFromThis();
            }
            _slot.ChangingModul -= OnModulChange;
        }
	    //get data when draged modul vis change
        private void OnModulChange(NamesOfModuls value)
        {
            _weaponData = PrimalClass.Singleton.GetWEaponData();
            lineRender.color = Color.yellow;
            if (value.ToString().Contains(RuneType.Main.ToString()) && int.Parse(this.gameObject.name)==0)
            {
                GetRuneID(value);
                ConectionLenghtLeft = _weaponData.runes[runeID].lenght;
            }
            else if (value.ToString().Contains(RuneType.Distributor.ToString()) && ConectionLenghtLeft>1)
            {
                DistributeConnection(int.Parse(this.gameObject.name), 1);
                GetRuneID(value);
            }
            else if (value == NamesOfModuls.empty)
            {
                if (runeTypeName == RuneType.Main && int.Parse(this.gameObject.name) == 0)
                { ConectionLenghtLeft = 0; }
                if(runeTypeName==RuneType.Distributor)
                {
                    DistributeConnection(int.Parse(this.gameObject.name), 1,false);
                }
                runeTypeName = RuneType.Empty;
                lineRender.color = Color.magenta;
            }
            else
            {
                GetRuneID(value);
            }
            WeaponRunesEq.Singleton.GetVisForPosWepMade().CheckIfCanMadeWeapon(false);
        }
	    //DistributeConnection connection to left and right parts of grid if needed
        private void DistributeConnection(int name,int _lenght,bool activethandesactiv=true)
        {
            //first all on left side
            for (int i = 1; i <= _lenght; i++)
            {
                if(name% Mathf.Pow(_unit, this.gameObject.name.Length - 1)/_unit==1)
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1)+i).ToString();

                }
                else if(int.Parse(this.gameObject.name)%_unit>=i)
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1) - i).ToString();
                }
                else
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1) - name % _unit
                        +_unit+(i- name % _unit)).ToString();
                }
                
                ActivDeactiveNextRune(_tempString, activethandesactiv);
            }
            //now all on right side
            for (int i = 1; i <= _lenght; i++)
            {
                if (name % Mathf.Pow(_unit, this.gameObject.name.Length - 1) / _unit == 0)
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1) + i).ToString();

                }
                else if (int.Parse(this.gameObject.name) % _unit >= i)
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1) - i).ToString();
                }
                else
                {
                    _tempString = (name + Mathf.Pow(_unit, this.gameObject.name.Length - 1) - name % _unit
                        - _unit + (i - name % _unit)).ToString();
                }

                ActivDeactiveNextRune(_tempString, activethandesactiv);
            }
        }
        //made connections between nods
        private RuneSlotNode _tempNode;
        private Vector3 _tempPointPos;
        private void ActivDeactiveNextRune(string name,bool activethandesactiv=true)
        {
            for (int i = 0; i < _parent.childCount; i++)
            {
                if(_parent.GetChild(i).gameObject.name==name)
                {
                        

                    if (activethandesactiv)
                    {
                        _tempNode = _parent.GetChild(i).GetComponent<RuneSlotNode>();
                        _tempNode.ConectionLenghtLeft = ConectionLenghtLeft - 1;
                        _tempNode.previusNode.Add(this.gameObject);
                        _tempPointPos = this.transform.localPosition - _tempNode.transform.localPosition;
                        _tempPointPos.y += this.GetComponent<RectTransform>().sizeDelta.y / 2-5;
                        _tempNode.lineRender.points[1] = _tempPointPos;
                        _tempNode.lineRender.color = Color.magenta;
                        nextNode.Add(_tempNode.gameObject);
                        
                    }
                    else if (_parent.GetChild(i).GetComponent<RuneSlotNode>().previusNode.Contains(this.gameObject))
                    {
                        _tempNode = _parent.GetChild(i).GetComponent<RuneSlotNode>();
                        _tempNode.previusNode.Remove(this.gameObject);
                        _tempNode.lineRender.points[1] = _tempNode.lineRender.points[0];
                        if (_tempNode.previusNode.Count == 0)
                        {
                            _tempNode.ConectionLenghtLeft = 0;
                        }
                        nextNode.Remove(_tempNode.gameObject);
                    }
                }
            }
        }
	    //get id of modul in scripObj
        private void GetRuneID(NamesOfModuls value)
        {
            for (int i = 0; i < _weaponData.runes.Length; i++)
            {
                if (_weaponData.runes[i].name == value)
                {
                    runeID = i;
                    runeTypeName = _weaponData.runes[i].model;
                }
            }
        }

    }
}