﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DoomWave
{
	//refill hp and ammo and add one more enemy for next gen room
    public class RefilHPAndAmmo : MonoBehaviour
    {
        private PlayerData _playerData;
        private Button _button;
        private ModifiersData _mods, _modsGrowth;
        public float value=0.3f;
        public bool byProcent = true, onlyMissing = true,addEnemy=true;
    void Start()
        {
            
            if (this.gameObject.TryGetComponent(out _button))
            {
                _button.onClick.AddListener(() => { RefilHpAndAmmo(value, byProcent, onlyMissing, addEnemy); });

            }
        }
        public void RefilHpAndAmmo(float value,bool byProcent=false, bool onlymissing=true,bool addEnemy=true)
        {
            //refil by procent value
            _playerData = PrimalClass.Singleton.GetPlayerData();
            _mods = PrimalClass.Singleton.GetModifiersData();
            _modsGrowth = PrimalClass.Singleton.GetModifiersDataGrowth();
            if ( byProcent)
            {
                value = value > 1 ? 1 : value;
                if(onlymissing)
                {
	                _playerData.data.hp +=Mathf.CeilToInt( (_playerData.data.maxHp - _playerData.data.hp) * value);
                    _playerData.data.manaStack += Mathf.CeilToInt((_playerData.data.maxManaStack - _playerData.data.manaStack) * value);
                }else
                {
                    _playerData.data.hp += Mathf.CeilToInt((_playerData.data.maxHp) * value);
                    _playerData.data.manaStack += Mathf.CeilToInt((_playerData.data.maxManaStack) * value);
                }
                
            }
            //ifByNumber
            if(!byProcent)
            {
                _playerData.data.hp += Mathf.CeilToInt((_playerData.data.maxHp) + value);
                _playerData.data.manaStack += Mathf.CeilToInt((_playerData.data.maxManaStack) + value);
            }

            //lowerToMax
            if (_playerData.data.hp > _playerData.data.maxHp)
            {
                _playerData.data.hp = _playerData.data.maxHp;
            }
            if (_playerData.data.manaStack > _playerData.data.maxManaStack)
            {
                _playerData.data.manaStack = _playerData.data.maxManaStack;
            }

            if(addEnemy)
            {
                _mods.lineralData.enemyNumber += _modsGrowth.lineralData.enemyNumber;
            }
	        TakingDmg.OnhpChange();
        }

    }
}