﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace DoomWave
{
    public class RewardWindow : MonoBehaviour
    {
        public enum CheckForError
        {
            UpgradeNotTaken,WeaponNotBuilded
        }
        private string _error = "Error";
        [SerializeField] GameObject _mainObject, _weaponSettings, _exitButton, _others;
        [SerializeField] private Button[] _option;
        [SerializeField] private GameObject _upgradesStack;
        private List<BaseUpgrade>[] _upgradesPlayer;
        private GameObject _player;
        private GameObject _primalWeapon,_genRuneStorage;
        [SerializeField] private WeaponData _weaponData;
        [SerializeField] private BaseEnemiesStat _enemiesData;
        [SerializeField] private ModifiersData _modsData, _modsGrowData;
        [SerializeField] private PlayerData _playerData;
        private RuneControler _runeContr;
        private TextMeshProUGUI _errorExitText;


        private TextMeshProUGUI[] _textoption;
        //add reward system and scripObjcs to singleton
        private void Awake()
        {
            PrimalClass.Singleton.SetRewardSystem(this.gameObject);
            PrimalClass.Singleton.SetScriptableObjects(_weaponData, _enemiesData, _modsData, _modsGrowData, _playerData);
        }
        void Start()
        {

            _textoption = new TextMeshProUGUI[_option.Length];
            for (int i = 0; i < _option.Length; i++)
            {
                _textoption[i] = _option[i].transform.GetComponentInChildren<TextMeshProUGUI>();
            }
            //_errorExitText = _exitButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            for (int i = 0; i < _exitButton.transform.childCount; i++)
            {
                if (_exitButton.transform.GetChild(i).name.Contains(_error))
                {
                    _errorExitText = _exitButton.transform.GetChild(i).GetComponent<TextMeshProUGUI>();
                }
            }
            _errorExitText.text = "";
            _mainObject.SetActive(false);
            _weaponSettings.SetActive(false);
            _exitButton.SetActive(false);
            _others.SetActive(false);
            _upgradesPlayer = new List<BaseUpgrade>[_upgradesStack.transform.childCount];
            for (int i = 0; i < _upgradesStack.transform.childCount; i++)
            {
                _upgradesPlayer[i] = new List<BaseUpgrade>();
                _upgradesStack.transform.GetChild(i).GetComponentsInChildren(false, _upgradesPlayer[i]);
            }
            _player = PrimalClass.Singleton.GetPlayer();
            _primalWeapon = _player.GetComponentInChildren<RuneControler>(true).gameObject;
            _genRuneStorage = WeaponRunesEq.Singleton.GetGenRuneStorage();
            
            _exitButton.GetComponent<Button>().onClick.AddListener(ExitButton);

        }
        //deactivate reward system and back to fight mode
        private void ExitButton()
        {
            if (!_mainObject.activeSelf && _primalWeapon.transform.childCount>1)
            {
                _errorExitText.text = "";
                Time.timeScale = 1;
                _mainObject.SetActive(false);
                _weaponSettings.SetActive(false);
                _exitButton.SetActive(false);
                _others.SetActive(false);
                _player.GetComponent<UnityEngine.InputSystem.PlayerInput>().enabled = true;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = false;
                //hide weapon if needed
                _runeContr = _runeContr == null ? _runeContr = PrimalClass.Singleton.GetPlayer().GetComponentInChildren<RuneControler>() : _runeContr;
                if (PrimalClass.Singleton.GetPlayer().GetComponentInChildren<SwitchWeapon>().IDWeapon == 0)
                    _runeContr.HideWeapon(_runeContr._readyToFire);

            }
            else if(_mainObject.activeSelf)
            {
                _errorExitText.text = CheckForError.UpgradeNotTaken.ToString();
            }else
            {
                _errorExitText.text = CheckForError.WeaponNotBuilded.ToString();
            }
        }
        private int _numberOfOption, _selectedNumber, _selectOptions, _selectNegativeOtions, _numberOfNegativeOptions;
        IEnumerator RepatSetCursor()//dont ask why.i dont whant to know why, but its work witch this cursed cursor class (to some level-not fully)
        {
            yield return new WaitForEndOfFrame();
            Cursor.visible = true;
            yield return new WaitForEndOfFrame();
            Cursor.visible = true;
        }
        public void SelectReward()
        {
            //activate reward mode witch option to rebuild weapon
            _player.GetComponent<UnityEngine.InputSystem.PlayerInput>().enabled = false;
            _selectOptions = Random.Range(0, _upgradesStack.transform.childCount - 1);
            _numberOfOption = _upgradesPlayer[_selectOptions].Count;
            _numberOfNegativeOptions = _upgradesPlayer[_upgradesStack.transform.childCount - 1].Count;
            Cursor.lockState = CursorLockMode.Confined;
            StartCoroutine(RepatSetCursor());
            Time.timeScale = 0;
            _mainObject.SetActive(true);
            _weaponSettings.SetActive(true);
            _exitButton.SetActive(true);
            _others.SetActive(true);
            //generate rewards witch their minus effects
            for (int i = 0; i < _option.Length; i++)
            {
                _selectedNumber = Random.Range(0, _numberOfOption);
                _selectNegativeOtions = Random.Range(0, _numberOfNegativeOptions);
                _textoption[i].text = null;
                _upgradesPlayer[_selectOptions][_selectedNumber].SetData(_option[i], _textoption[i], _weaponSettings, _mainObject);
                _upgradesPlayer[_upgradesStack.transform.childCount - 1][_selectNegativeOtions].SetData(_option[i], _textoption[i], _weaponSettings, _mainObject);
            }
            _genRuneStorage.transform.GetChild(2).gameObject.SetActive(true);
            _genRuneStorage.transform.GetChild(3).gameObject.SetActive(true);
        }
        public void CreateFirstWeapon()
        {
            _primalWeapon.GetComponentInParent<SwitchWeapon>().WeaponsUnBlock += 1;
            _player.GetComponent<UnityEngine.InputSystem.PlayerInput>().enabled = false;
            Cursor.lockState = CursorLockMode.Confined;
            StartCoroutine(RepatSetCursor());
            Time.timeScale = 0;
            _weaponSettings.SetActive(true);
            _exitButton.SetActive(true);
            _genRuneStorage.transform.GetChild(0).gameObject.SetActive(true);
            _genRuneStorage.transform.GetChild(1).gameObject.SetActive(true);
        }
        
    }
}