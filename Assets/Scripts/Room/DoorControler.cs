﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class DoorControler : MonoBehaviour
    {
        [SerializeField] GameObject _doorLeft, _doorRight, _signal, _detector;
        private DoorPlayerDet _detecClass;
        private bool _readyToOpen = true,_open=false;
        
        public delegate void DoorToOpen();
        public DoorToOpen openDoor;
        public delegate void DoorToClose();
        public DoorToClose closeDoor;
        public delegate void ReadyDoorToOpen();
        public ReadyDoorToOpen readyOpenDoor;
        public delegate void UnReadyDoorToOpen();
        public UnReadyDoorToOpen unreadyOpenDoor;
        public bool ReadyToOpen {
            get
            {
                return _readyToOpen;
            }
            set
            {
                _readyToOpen = value;
                if(value)
                {
                    readyOpenDoor();
                }
                else
                {
                    unreadyOpenDoor();
                }
            }
        }
        private void OpenIfCan()
        {
            if(ReadyToOpen)
            {
                openDoor();
            }
        }
        private void CloseIfNeeded()
        {
                closeDoor();
            CheckIfOutsideOfRoom();
        }
        private void CheckIfOutsideOfRoom()
        {
            //Debug.Log(PrimalStat.InverseLerp(Vector3.zero, this.transform.localPosition, this.transform.parent.InverseTransformPoint(PrimalStat.Singleton.GetPlayer().transform.position)));
           if( PrimalClass.InverseLerp(Vector3.zero, this.transform.localPosition,this.transform.parent.InverseTransformPoint(PrimalClass.Singleton.GetPlayer().transform.position))>1)
            {
                ReadyToOpen = false;
            }
        }
        // Start is called before the first frame update
        private void OnEnable()
        {
            _detecClass.playerIsClose += OpenIfCan;
            _detecClass.playerIsNotClose += CloseIfNeeded;
        }
        
        private void OnDisable()
        {
            _detecClass.playerIsClose -= OpenIfCan;
            _detecClass.playerIsNotClose -= CloseIfNeeded;
        }
        void Awake()
        {
            _detecClass = _detector.GetComponent<DoorPlayerDet>();

        }


    }
}