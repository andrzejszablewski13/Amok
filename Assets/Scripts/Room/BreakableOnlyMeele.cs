﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class BreakableOnlyMeele : MonoBehaviour,IHitByMeele
    {
        public void TakeDmgClose(int value)
        {
            Destroy(this.gameObject);
        }

    }
}