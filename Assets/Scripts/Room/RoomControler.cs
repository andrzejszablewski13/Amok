﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{

    public class RoomControler : MonoBehaviour
    {

        // Start is called before the first frame update
        public delegate void EnemyDeath();
        public EnemyDeath enemyDie;
        public GameObject door, enemies,walls,flor,roof;
        [HideInInspector]public DoorControler doorClass;
        public bool getReward = false;
        public RewardOptions rewardType;
        public delegate void PlayerComeIN();
        public PlayerComeIN PlayerInside;
        public bool playerInThisRoom = false;

        
        void Start()
        {
            doorClass = door.gameObject.GetComponent<DoorControler>();
            if (enemies.transform.childCount > 0)
            {
                doorClass.ReadyToOpen = false;
            }
        }
        private string tagPlayer= "Player";
        private void OnTriggerEnter(Collider other)
        {

            if (other.gameObject.tag == tagPlayer)
            {
                PlayerInside();
                
            }
        }
        private void PlayerHere()
        {
            playerInThisRoom = true;
        }
        private void OnEnable()
        {
            enemyDie += TestIfStillEnemiesAlive;
            PlayerInside += PlayerHere;
        }
        private void OnDisable()
        {
            enemyDie -= TestIfStillEnemiesAlive;
            PlayerInside -= PlayerHere;
        }

        private void TestIfStillEnemiesAlive()
        {
            if (enemies.transform.childCount <= 0)
            {
                Reward();
            }
        }
        private void Reward()
        {

            if (getReward)
            switch (rewardType)
            {
                case RewardOptions.smallUpgrade:
                        PrimalClass.Singleton.GetRewardSystem().GetComponent<RewardWindow>().SelectReward();
                    break;
                case RewardOptions.BigUpgrade:
                    break;
                case RewardOptions.newWeapon:
                    //PrimalClass.Singleton.GetPlayer().transform.GetChild(0).GetChild(0).GetComponent<SwitchWeapon>().WeaponsUnBlock += 1;
                        PrimalClass.Singleton.GetRewardSystem().GetComponent<RewardWindow>().CreateFirstWeapon();
                        break;
                default:
                    break;
            }

            doorClass.ReadyToOpen = true;
        }

    }
}