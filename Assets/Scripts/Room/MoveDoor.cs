﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class MoveDoor : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] [Range(0, 20f)] private float _howMuchOpen = 0.5f;
        [SerializeField] DoorControler _doorControler;
        private string _left = "Left";
        private bool _open = false;
        void OnEnable()
        {
            _doorControler.openDoor += OpenDoor;
            _doorControler.closeDoor += CloseDoor;
        }
        private void OnDisable()
        {
            _doorControler.openDoor -= OpenDoor;
            _doorControler.closeDoor -= CloseDoor;
        }

        private void OpenDoor()
        {
            if (!_open)
            {
                if (this.gameObject.name.Contains(_left))
                {
                    this.transform.Translate(Vector3.left * this.transform.localScale.x * _howMuchOpen);
                }
                else
                {
                    this.transform.Translate(Vector3.right * this.transform.localScale.x * _howMuchOpen);
                }
                _open = true;
            }
        }
        private void CloseDoor()
        {
            if (_open)
            {
                if (this.gameObject.name.Contains(_left))
                {
                    this.transform.Translate(Vector3.right * this.transform.localScale.x * _howMuchOpen);
                }
                else
                {
                    this.transform.Translate(Vector3.left * this.transform.localScale.x * _howMuchOpen);
                }
                _open = false;
            }

        }
    }
}