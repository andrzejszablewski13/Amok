﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signal : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Material _ready, _unReady;
    private MeshRenderer _render;
    [SerializeField] private DoorControler _doorControler;
    private void OnEnable()
    {
        _render = this.GetComponent<MeshRenderer>();
        _doorControler.openDoor += OnDoorOpen;
        _doorControler.closeDoor += OnDoorClose;
        _doorControler.unreadyOpenDoor += OnUnreadyToOpen;
        _doorControler.readyOpenDoor += OnReadyToOpen;
    }
    private void OnDisable()
    {
        _doorControler.openDoor -= OnDoorOpen;
        _doorControler.closeDoor -= OnDoorClose;
        _doorControler.unreadyOpenDoor -= OnUnreadyToOpen;
        _doorControler.readyOpenDoor -= OnReadyToOpen;
    }
    

    private void OnDoorOpen()
    {
        _render.enabled = false;
    }
    private void OnDoorClose()
    {
        _render.enabled = true;
    }
    private void OnReadyToOpen()
    {
        _render.material = _ready;
    }
    private void OnUnreadyToOpen()
    {
        _render.material = _unReady;
    }

}
