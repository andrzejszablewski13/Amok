﻿using DoomWave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
    public class DoorPlayerDet : MonoBehaviour
    {
        public delegate void OnPlayerClose();
        public OnPlayerClose playerIsClose;
        public delegate void OnPlayerNotClose();
        public OnPlayerNotClose playerIsNotClose;
        private bool _playerIsClose;
        public bool PlayerIsClose
        {
            get { return _playerIsClose; }
        }

        private string _player = "Player";
        // Start is called before the first frame update
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == _player)
            {
                _playerIsClose = true;
                playerIsClose();
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == _player)
            {
                _playerIsClose = false;
                playerIsNotClose();
            }
        }
    }
}