﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoomWave
{
	//enum witch names of spawnpools names
    public enum PopularStrings
    {
        Player,WeaponParts, Rooms,Spawner, CanvasSpawnPool
    }
	//name of moduls types
    [Serializable]
    public enum RuneType
    {
        Empty,Main, End,Recycler,Distributor,AmplifiyerDMG,Speeder,Controler,KnockOutRun
    }
	//names of moduls prefabs-need to updata manually
    [Serializable]
    public enum NamesOfModuls
    {
	    empty,Main1, End1, ManaRecycler1, ManaDistributor1
    }
	//to take hit only by range
    interface IHitByRange
    {
        void TakeDmg(int value);
    }
	//to take only hit only by meele
    public interface IHitByMeele
    {
        void TakeDmgClose(int value);
    }
    
    [Serializable]
    public enum RewardOptions
    {
        smallUpgrade, BigUpgrade, newWeapon
    }
    public class PrimalClass
    {
        // Start is called before the first frame update
        private static PrimalClass _primalSingleton;
        private static GameObject _player;
        private static GameObject _rewardSystem;
        private static WeaponData _weaponData;
        private static BaseEnemiesStat _enemiesData;
        private static ModifiersData _modsData, _modsGrowData;
        private static PlayerData _playerData;
        public static PrimalClass Singleton
        {
            get
            {
                if(_primalSingleton==null)
                {
                    _primalSingleton = new PrimalClass();
                }
                return _primalSingleton;
            }
        }
        public void SetScriptableObjects(WeaponData weaponData, BaseEnemiesStat enemiesData, ModifiersData modsData, ModifiersData modsGrowData, PlayerData playerData)
        {
            _weaponData = weaponData;
            _enemiesData = enemiesData;
            _modsData = modsData;
            _modsGrowData = modsGrowData;
            _playerData = playerData;
        }
        public WeaponData GetWEaponData()
        {
            return _weaponData;
        }
        public BaseEnemiesStat GetBaseEnemiesStat()
        {
            return _enemiesData;
        }
        public ModifiersData GetModifiersData()
        {
            return _modsData;
        }
        public ModifiersData GetModifiersDataGrowth()
        {
            return _modsGrowData;
        }
        public PlayerData GetPlayerData()
        {
            return _playerData;
        }
        public void SetRewardSystem(GameObject _rew)
        {
            _rewardSystem = _rew;
        }
        public GameObject GetRewardSystem()
        {
            return _rewardSystem;
        }
        public GameObject GetPlayer()
        {
            return _player;
        }
        public void SetPlayer(GameObject player)
        {
            _player = player;
        }
        public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
        {
            Vector3 AB = b - a;
            Vector3 AV = value - a;
            return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
        }
    }
}